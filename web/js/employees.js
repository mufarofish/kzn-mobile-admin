// Users javascript

function addEmployee() {
	        var name=$('#addempname').val();
			var email=$('#addempemail').val();
			var dpt=$('#addempdpt').val();
	        var gender=$('#addempgender').val();
			var grade=$('#addempsalarygrade').val();
			var pass=$('#addempid').val();
	        var phone=$('#addempphone').val();
	        var empadr=$('#addempadr').val();
	        var ecn=$('#addempecn').val();
	        var rtype="add";	
			 if ((/^\s*$/).test(name)) {
		     Materialize.toast("Enter name please", 3000);      
             }
			 else
			 if ((/^\s*$/).test(email)) {
		     Materialize.toast("Enter email please", 3000);           
             }			
			else{
			 $.post("controllers/EmployeeController.php",{
			 rtype: rtype,
			 name: name,
			 dpt: dpt,
             gender:gender,
			 email:email,
			 pass: pass,
			 phone:phone,
			 empadr:empadr,
			 ecn:ecn,
			 grade:grade
			 },
			 function(response,status){ // Required Callback Function  		 
			 var stat=response["stat"];
			 var msg=response["msg"];
			 if(stat=="OK"){
			 Materialize.toast("Employee created successfully", 3000, '',function(){  ajaxGetSavedEmployees(); });  
			 }
			 else{
			 Materialize.toast("Error! " + msg, 6000);      
			 }

			 }		
			
          )};
   
}

function deleteEmployee(value, object) {
	var rtype="delete";
	var a = confirm("Are you sure to delete this record?");
     if(a) {
$.post("controllers/EmployeeController.php",{
       id: value,
	   rtype: rtype
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetSavedEmployees();
       }    
)
}
}
function ajaxGetSavedEmployees(){
   var rtype="view";
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype
    },
    url: "controllers/EmployeeController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
	  var count=0;
      // Loop through Object and create peopleHTML
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			count++
       responsesHTML += "<tr>";
	   responsesHTML += "<td>" + JSONObject[key]["ecn"] + "</td>";
       responsesHTML += "<td>" + JSONObject[key]["name"] + "</td>";
	   responsesHTML += "<td>" + JSONObject[key]["pass"] + "</td>";
	   responsesHTML += "<td>" + JSONObject[key]["gender"] + "</td>";
       responsesHTML += "<td>" + JSONObject[key]["email"] + "</td>";
       responsesHTML += "<td>" + JSONObject[key]["phone"] + "</td>";
       responsesHTML += "<td>" + JSONObject[key]["dpt"] + "</td>";	
	   responsesHTML += "<td>" + JSONObject[key]["grade"] + "</td>";	  
	responsesHTML += "<td onClick='deleteEmployee("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Employee'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#employeesListDiv").html(responsesHTML);
    }
  });
     //End of ajax
}
function ajaxGetEmpListPayslips(){
   var rtype="view";
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype
    },
    url: "controllers/EmployeeController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
	  var count=0;
      // Loop through Object and create peopleHTML
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			count++
       responsesHTML += "<tr>";
	   responsesHTML += "<td>" + JSONObject[key]["ecn"] + "</td>";
       responsesHTML += "<td>" + JSONObject[key]["name"] + "</td>";
	   responsesHTML += "<td>" + JSONObject[key]["pass"] + "</td>";
	   responsesHTML += "<td>" + JSONObject[key]["gender"] + "</td>";
       responsesHTML += "<td>" + JSONObject[key]["email"] + "</td>";
       responsesHTML += "<td>" + JSONObject[key]["phone"] + "</td>";
       responsesHTML += "<td>" + JSONObject[key]["dpt"] + "</td>";	
	   responsesHTML += "<td>" + JSONObject[key]["grade"] + "</td>";	  
	responsesHTML += "<td onClick='openEmployeePayslips(\""+JSONObject[key]["name"]+"\", \""+JSONObject[key]["ecn"]+"\", \""+JSONObject[key]["phone"]+"\", \""+JSONObject[key]["email"]+"\", \""+JSONObject[key]["pass"]+"\", \""+JSONObject[key]["grade"]+"\")' style='cursor:pointer;'><i class='fa fa-files-o' data-toggle='tooltip' title='Open/Create Payslips'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#employeesListPayslipsDiv").html(responsesHTML);
    }
  });
     //End of ajax
}