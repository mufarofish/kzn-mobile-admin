<?php
header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
header('Content-type: application/json');
include '../config/DB.php';
include 'CypherController.php';
include 'ObjectValueController.php';
$db = new DB();
$tblName = 'press';
$type = $_GET['rtype'];
$_POST = json_decode(file_get_contents("php://input"), true);
if(isset($type) && !empty($type)){
    switch($type){
        case "view":
			$orderby='created DESC';
			$filterArray = array('order_by'=>$orderby);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
		case "viewbyid":
			$orderby='created DESC';
			if(isset($_POST['status'])){
				$where=array('status'=>$_POST['status'], 'branch'=>$_SESSION['branch']);	
			}
			if(isset($_POST['id'])){
				$where=array('id'=>$_POST['id']);	
			}
			$filterArray = array('where' => $where, 'order_by'=>$orderby);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
			
		case "viewbyidrpt":
			$orderby='created DESC';
			$where=array('status'=>$_POST['status'], 'branch'=>$_SESSION['branch']);	
			$filterArray = array('where' => $where, 'order_by'=>$orderby);
            $records = $db->getRowsInvoiceRtp($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRowsInvoiceRtp($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
			
		case "search":
			$orderby='created DESC';
			$where=array('status'=>$_POST['status'], 'branch'=>$_SESSION['branch'], 'clientid'=>$_POST['filterText']);	
			$filterArray = array('where' => $where, 'order_by'=>$orderby);
            $records = $db->getFilterRowsBranch($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getFilterRowsBranch($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
	    case "rptsearch":
			$orderby='created DESC';
			$where=array('status'=>$_POST['status'], 'branch'=>$_SESSION['branch'], 'clientid'=>$_POST['filterText']);	
			$filterArray = array('where' => $where, 'order_by'=>$orderby);
            $records = $db->getRptFilter($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRptFilter($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
			
		case "rptdates":
			$sdate=$_POST['sdate'];
			$edate=$_POST['enddate'];
            $records = $db->getRptFilterDates($sdate, $edate);
            if($records){
                $data['records'] = $db->getRptFilterDates($sdate, $edate);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
			
        case "add":     
               $postData = array(
                    'clientid' => $_POST['clientid'],
                    'branch' => $_SESSION['branch'],
					'jobdescr' => $_POST['jobdescr'],
				    'cid' => $_POST['cid'],
                    'ref' => $_POST['jref'],
					'carid' => $_POST['carid'],
				    'jdays' => $_POST['jdays'],
				    'otherinfo' => $_POST['otherinfo'],
				    'email'=>$_POST['email'],
					'doneby' => $_SESSION['nam']
                );
			  if($postData){
                $insert = $db->insert($tblName,$postData);
                if($insert){
                    $data['data'] = $insert;
                    $data['stat'] = 'OK';
                    $data['msg'] = 'Job card created successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
        case "edit":	    
                $updateData = array(
                    'status' => $_POST['setto']
                );
			   if($updateData){
                $condition = array('id' => $_POST['id']);
                $update = $db->update($tblName,$updateData,$condition);
                if($update){
                    $data['stat'] = 'OK';
                    $data['msg'] = 'Job card updated successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
        case "delete":           
              $condition = array('id' => $_POST['id']);
			  if($condition){
                $delete = $db->delete($tblName,$condition);
                if($delete){
                    $data['stat'] = 'OK';
                    $data['msg'] = 'Job card deleted successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
        default:
            echo '{"status":"INVALID"}';
    }
}