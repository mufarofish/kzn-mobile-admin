<?php
session_start();
error_reporting(0);
if(!$_SESSION['usr']){
die('<script language=javascript>alert("You are not logged. Please do not try to hack"); location=("index.html");</script>');
}
?>
<!doctype html>
<html lang="en">

<head>
	<title>KZN</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
  <!-- Compiled and minified CSS -->
  <!-- Compiled and minified JavaScript -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor/icon-sets.css">
	<link rel="stylesheet" href="assets/css/main.css">
	<link rel="stylesheet" href="css/override.css">
	<link rel="stylesheet" href="css/prints.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
    <link rel="stylesheet" href="libs/css/alertify.default.css">
	<link rel="stylesheet" href="libs/css/alertify.core.css">
	<link href="uploader/assets/css/style.css" rel="stylesheet" />
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<div class="sidebar">
			<div class="brand" style="padding: 11px;">
				<a href="dashboard.php"><img src="AppImages/mainlogo.png" alt="KZN" class="img-responsive" height="20px" style="width: 55%;margin-left: 11%;"></a>
			</div>
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li><a href="#" class="" onClick="loadDashboard()"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
						<li><a href="#" class="" id="vusers" onClick="loadUsers()"><i class="lnr lnr-user"></i> <span>App Admins</span></a></li>				
						<li><a href="#" class="" onClick="loadAppUsers()"><i class="lnr lnr-users" ></i> <span>App Users</span></a></li>
						<li><a href="#" class="" onClick="loadViewShowcase()"><i class="lnr lnr-film-play" ></i> <span>App ShowCase</span></a></li>
						<li>
							<a href="#subPagesCards" data-toggle="collapse" class="collapsed"><i class="lnr lnr-list"></i> <span>Inquiries</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPagesCards" class="collapse ">
								<ul class="nav">
							        	<li><a href="#" class="" onClick="loadViewBursaryInquiry()"><i class="lnr lnr-graduation-hat" ></i> <span>Bursary</span></a></li>
						                <li><a href="#" class="" onClick="loadViewFundingInquiry()"><i class="lnr lnr-book" ></i> <span>Funding</span></a></li>
						                <li><a href="#" class="" onClick="loadViewLocationInquiry()"><i class="lnr lnr-select" ></i> <span>Location</span></a></li>
						                <li><a href="#" class="" onClick="loadViewFacilityInquiry()"><i class="lnr lnr-apartment" ></i> <span>Facilities</span></a></li>
					                   <li><a href="#" class="" onClick="loadViewTrainingInquiry()"><i class="lnr lnr-film-play" ></i> <span>Training</span></a></li>
					                   <li><a href="#" class="" onClick="loadViewCrewInquiry()"><i class="lnr lnr-users" ></i> <span>Crew</span></a></li>
								</ul>
							</div>
						</li>
						<li><a href="#" class="" onClick="loadViewLocation()"><i class="lnr lnr-map" ></i> <span>Film Locations</span></a></li>	
						<li><a href="#" class="" onClick="loadViewEvents()"><i class="lnr lnr-calendar-full" ></i> <span>News &amp; Events </span></a></li>
						<li><a href="#" class="" onClick="loadViewSocial()"><i class="lnr lnr-briefcase" ></i> <span>Corporate &amp; Social Inv</span></a></li>
						<li><a href="#" class="" onClick="loadViewFacility()"><i class="lnr lnr-apartment" ></i> <span>Facilities on Offer</span></a></li>				
					</ul>
				</nav>
			</div>
		</div>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
							<span class="sr-only">Toggle Navigation</span>
							<i class="fa fa-bars icon-nav"></i>
						</button>
					</div>
					<div id="navbar-menu" class="navbar-collapse collapse">
						<form class="navbar-form navbar-left hidden-xs">
							<div class="input-group">
								<input type="text" value="" class="form-control" placeholder="Search dashboard...">
								<span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
							</div>
						</form>
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
									<i class="lnr lnr-alarm"></i>
									<span class="badge bg-danger">1</span>
								</a>
								<ul class="dropdown-menu notifications">
									<li><a href="#" class="notification-item"><span class="dot bg-success"></span>Welcome <?php echo $_SESSION['nam']; ?></a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="lnr lnr-question-circle"></i> <span>Help</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="#" onclick="manualMessage()">Basic Use</a></li>	
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/user.png" class="img-circle" alt="Avatar" width="25px" height="25px"> <span><?php echo $_SESSION['nam']; ?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="index.html"><i class="lnr lnr-cog"></i> <span>Lock Screen</span></a></li>
									<li><a href="index.html"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!--ADD USER -->
					<div class="row" id="userPanel">
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">App Admin</h3>
								</div>
								<div class="panel-body">
								<div class="row">
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Name" id="name"></div>
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Email" id="email"></div>
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Phone" id="phone"></div>
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Username" id="username"></div>
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Password" id="password"></div>
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Confirm Password" id="confirm"></div>
								<div class="col-md-6">
								<button type="button" class="btn btn-primary btn-block" id="createUser" onClick="saveUser()">Create</button>
								</div>
								</div>
								</div>
							</div>
						</div>
							<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">App Admins Lists</h3>
								</div>
								<div class="panel-body">
								<div class="row">
					             <table class="table">
										<thead>
											<tr>
												<th>#</th>
												<th>Name</th>
												<th>Email</th>
												<th>Phone</th>
												<th></th>
											</tr>
										</thead>
										<tbody id="usersListDiv">
											
										</tbody>
									</table>			             
								</div>
								</div>
							</div>
						</div>
					</div>	
					<!--ADD CLIENT-->
					<div class="row" id="appUsersPanel">
						
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading search-header">
									<div class="col-md-6">App Users</div>
									<div class="col-md-3">
									<select class="form-control" id="astatusers">
									    <option value="404" selected>--Filter By Status--</option>
										<option value="1">Active Users</option>
										<option value="0">Inactive Users</option>
									</select>
									</div>
									<div class="col-md-3 search-header2">
									<div class="input-group apm-text-field">
										<span class="input-group-addon"><i class="fa fa-search"></i></span>
										<input type='text' class="form-control apm-text-field" placeholder="Search" id="searchClient" />
									</div> 
								   </div> 
								</div>
								<div class="panel-body">
								<div class="row">
					             <table class="table" id="clientsTable">
										<thead>
											<tr>
												<th>#</th>
												<th>Company</th>
												<th>Name</th>
												<th>Email</th>
												<th>Phone</th>
												<th>Status</th>
												<th>Newsletter</th>
												<th>Registered</th>
												<th></th>
											</tr>
										</thead>
										<tbody id="appUsersListDiv">
											
										</tbody>
									</table>			             
								</div>
								</div>
							</div>
						</div>
					</div>
					<!--SOCIAL AND COP EVENTS -->
					<div class="row" id="editSocialPanel">
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Edit Corporate &amp; Social Investments News</h3>
								</div>
								<div class="panel-body">
								<div class="row">
								<div class="col-md-4"><input type="text" class="form-control apm-text-field" placeholder="Headline" id="txteditsocialheadlines"></div>
								<div class="col-md-4"><input type="text" class="form-control apm-text-field" placeholder="Subtitle" id="txteditsocialsubtitle"></div>
								<div class="col-md-4">
									<select class="form-control" id="txteditsocialcategory">
									    <option value="404" selected>--Select Option--</option>
										<option value="CSI Initiatives">CSI Initiatives</option>
										<option value="Upcoming CSI Initiatives">Upcoming CSI Initiatives</option>
									</select>
								</div>
								<div class="col-md-12"><textarea class="form-control apm-text-field" placeholder="Main Context/News" id="txteditsocialcontextinfor" rows="4"></textarea></div>							
								<div class="col-md-6">
								<button type="button" class="btn btn-primary btn-block" id="editSocialBtn" onClick="updateSocialSocial()">Update</button>
								</div>
								</div>
								</div>
							</div>
						</div>
					</div>
                
                   	<div class="row" id="createSocialPanel">
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Post Corporate &amp; Social Investments News</h3>
								</div>
								<div class="panel-body">
								<div class="row">
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Headline" id="txtsocialheadlines"></div>
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Subtitle" id="txtsocialsubtitle"></div>
								<div class="col-md-6">
									<select class="form-control" id="txtsocialcategory">
									    <option value="CSI Initiatives" selected>--Select Option--</option>
										<option value="CSI Initiatives">CSI Initiatives</option>
										<option value="Upcoming CSI Initiatives">Upcoming CSI Initiatives</option>
									</select>
								</div>
								<div class="col-md-6">
							       <div id="csdropBox" class="apm-text-field"><p>Select background image on app</p></div>
							           <input type="file" name="fileInput" id="csfileInput" />
					            </div>
								<div class="col-md-12"><textarea class="form-control apm-text-field" placeholder="Main Context/News" id="txtsocialcontextinfor" rows="4"></textarea></div>							
								<div class="col-md-6">
								<button type="button" class="btn btn-primary btn-block" id="createSocialBtn" onClick="createSocialSocial()">Create</button>
								</div>
								</div>
								</div>
							</div>
						</div>
					</div>
					<!--Location Inquiry-->
					<div class="row" id="socialListPanel">
						
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading search-header">
									<div class="col-md-3">Corporate &amp; Social Investments</div>
									<div class="col-md-1"><i class="fa fa-plus-circle" onclick="loadCreateSocialSocial()" style="font-size: 36px;color: #F58A22;cursor: pointer;"></i></span></div>
									<div class="col-md-3">
									<select class="form-control" id="catsocial">
									    <option value="404" selected>--Select Option--</option>
										<option value="CSI Initiatives">CSI Initiatives</option>
										<option value="Upcoming CSI Initiatives">Upcoming CSI Initiatives</option>
									</select>
									</div>
									<div class="col-md-3">
									<select class="form-control" id="astatsocial">
									    <option value="404" selected>--Filter By Status--</option>
										<option value="1">Active/Showing on App</option>
										<option value="0">Inactive/Showcase Request</option>
									</select>
									</div>
									<div class="col-md-2 search-header2">
									<div class="input-group apm-text-field">
										<span class="input-group-addon"><i class="fa fa-search"></i></span>
										<input type='text' class="form-control apm-text-field" placeholder="Search" id="searchSocial" />
									</div> 
								   </div> 
								</div>
								<div class="panel-body">
								<div class="row">
					             <table class="table" id="socialTable">
										<thead>
											<tr>
												<th>#</th>
												<th>Headline</th>
												<th>Subtitle</th>
												<th>Context</th>
												<th>Status</th>
												<th>Posted</th>
												<th>Toggle</th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
											</tr>
										</thead>
										<tbody id="socialListDiv">
											
										</tbody>
									</table>			             
								</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row" id="editSocialPicPanel">
                      <div class="col-md-10">
							 <div id="esdropBox"><p>Select image to upload</p></div>
							 <input type="file" name="fileInput" id="esfileInput" />
					   </div>
					  <div class="col-md-2">
							<input type="button" name="" class="btn btn-primary" value="SAVE" style="width: 100%; margin-top: 15px;" id="apply" onclick="saveEditSocialImage()" />
		             </div>
                   </div>

					<!--END OF SOCI AND COP-->

					<!--FACILITY FACILITY -->
                   <!--SOCIAL AND COP EVENTS -->
					<div class="row" id="editFacilityPanel">
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Facilities</h3>
								</div>
								<div class="panel-body">
								<div class="row">
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Facility Headline" id="txteditfacilityheadlines"></div>
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Subtitle" id="txteditfacilitysubtitle"></div>
								<div class="col-md-12"><textarea class="form-control apm-text-field" placeholder="Main Context/Information About Facility" id="txteditfacilitycontextinfor" rows="4"></textarea></div>							
								<div class="col-md-6">
								<button type="button" class="btn btn-primary btn-block" id="editFacilityBtn" onClick="updateFacilityFacility()">Update</button>
								</div>
								</div>
								</div>
							</div>
						</div>
					</div>
                
                   	<div class="row" id="createFacilityPanel">
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Facilities</h3>
								</div>
								<div class="panel-body">
								<div class="row">
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Facility Headline" id="txtfacilityheadlines"></div>
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Facility Subtitle" id="txtfacilitysubtitle"></div>
								<div class="col-md-12">
							       <div id="cfdropBox" class="apm-text-field"><p>Select background image on app</p></div>
							           <input type="file" name="fileInput" id="cffileInput" />
					            </div>
								<div class="col-md-12"><textarea class="form-control apm-text-field" placeholder="Main Context/Facility Information" id="txtfacilitycontextinfor" rows="4"></textarea></div>							
								<div class="col-md-6">
								<button type="button" class="btn btn-primary btn-block" id="createFacilityBtn" onClick="createFacilityFacility()">Create</button>
								</div>
								</div>
								</div>
							</div>
						</div>
					</div>
					<!--Location Inquiry-->
					<div class="row" id="facilityListPanel">
						
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading search-header">
									<div class="col-md-6">Facilities</div>
									<div class="col-md-1"><i class="fa fa-plus-circle" onclick="loadCreateFacilityFacility()" style="font-size: 36px;color: #F58A22;cursor: pointer;"></i></span></div>
									<div class="col-md-3">
									<select class="form-control" id="astatfacility">
									    <option value="404" selected>--Filter By Status--</option>
										<option value="1">Active/Showing on App</option>
										<option value="0">Inactive/Showcase Request</option>
									</select>
									</div>
									<div class="col-md-2 search-header2">
									<div class="input-group apm-text-field">
										<span class="input-group-addon"><i class="fa fa-search"></i></span>
										<input type='text' class="form-control apm-text-field" placeholder="Search" id="searchFacility" />
									</div> 
								   </div> 
								</div>
								<div class="panel-body">
								<div class="row">
					             <table class="table" id="facilityTable">
										<thead>
											<tr>
												<th>#</th>
												<th>Title</th>
												<th>Subtitle</th>
												<th>Context</th>
												<th>Status</th>
												<th>Posted</th>
												<th>Toggle</th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
											</tr>
										</thead>
										<tbody id="facilityListDiv">
											
										</tbody>
									</table>			             
								</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row" id="editFacilityPicPanel">
                      <div class="col-md-10">
							 <div id="efdropBox"><p>Select image to upload</p></div>
							 <input type="file" name="fileInput" id="effileInput" />
					   </div>
					  <div class="col-md-2">
							<input type="button" name="" class="btn btn-primary" value="SAVE" style="width: 100%; margin-top: 15px;" id="apply" onclick="saveEditFacilityImage()" />
		             </div>
                   </div>

                    <!-- END OF FACILITY FACILITY -->
						<!--ShowCase Inquiry-->
					<div class="row" id="showcaseListPanel">
						
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading search-header">
									<div class="col-md-6">Showcase</div>
									<div class="col-md-3">
									<select class="form-control" id="astatshowcase">
									    <option value="404" selected>--Filter By Status--</option>
										<option value="1">Active/Showing on App</option>
										<option value="0">Inactive/Showcase Request</option>
									</select>
									</div>
									<div class="col-md-3 search-header2">
									<div class="input-group apm-text-field">
										<span class="input-group-addon"><i class="fa fa-search"></i></span>
										<input type='text' class="form-control apm-text-field" placeholder="Search" id="searchShowcase" />
									</div> 
								   </div> 
								</div>
								<div class="panel-body">
								<div class="row">
					             <table class="table" id="showcaseTable">
										<thead>
											<tr>
												<th>#</th>
												<th>Film Name</th>
												<th>Director</th>
												<th>Main Actor</th>
												<th>Year</th>
												<th>Status</th>
												<th>Posted</th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
											</tr>
										</thead>
										<tbody id="showcaseListDiv">
											
										</tbody>
									</table>			             
								</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row" id="editShowcasePanel">
					<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Update Video</h3>
								</div>
								<div class="panel-body">
								<div class="row">
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Film Name" id="txteditshowcasefname"></div>
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Subtitle" id="txteditshowcasefdirector"></div>
								<div class="col-md-4"><input type="text" class="form-control apm-text-field" placeholder="Actor/s" id="txteditshowcasefactor"></div>
								<div class="col-md-4"><input type="text" class="form-control apm-text-field" placeholder="Year" id="txteditshowcasefyear"></div>
								<div class="col-md-4"><input type="text" class="form-control apm-text-field" placeholder="Youtube Link" id="txteditshowcaseflink"></div>
						
								<div class="col-md-6">
								<button type="button" class="btn btn-primary btn-block" id="editShowCaseBtn" onClick="updateFilmShocase()">Update</button>
								</div>
								</div>
								</div>
							</div>
						</div>

					</div>
					<div class="row" id="editNewsPanel">
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Update News/Events</h3>
								</div>
								<div class="panel-body">
								<div class="row">
								<div class="col-md-4"><input type="text" class="form-control apm-text-field" placeholder="Headline" id="txteditnewsheadlines"></div>
								<div class="col-md-4"><input type="text" class="form-control apm-text-field" placeholder="Subtitle" id="txteditnewssubtitle"></div>
								<div class="col-md-4">
									<select class="form-control" id="txteditnewscategory">
									    <option value="News" selected>--Select Option--</option>
									    <option value="Simon">Simon Sabela Film &amp; TV Awards</option>
										<option value="News">News</option>
										<option value="Events">Events &amp; Training </option>
									</select>
								</div>
								<div class="col-md-12"><textarea class="form-control apm-text-field" placeholder="Main Context/News" id="txteditnewscontextinfor" rows="4"></textarea></div>							
								<div class="col-md-6">
								<button type="button" class="btn btn-primary btn-block" id="editNewsBtn" onClick="updateNewsEvents()">Update</button>
								</div>
								</div>
								</div>
							</div>
						</div>
					</div>
                
                   	<div class="row" id="createNewsPanel">
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Post News/Events</h3>
								</div>
								<div class="panel-body">
								<div class="row">
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Headline" id="txtnewsheadlines"></div>
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Subtitle" id="txtnewssubtitle"></div>
								<div class="col-md-6">
									<select class="form-control" id="txtnewscategory">
									    <option value="News" selected>--Select Option--</option>
									     <option value="Simon">Simon Sabela Film &amp; TV Awards</option>
										<option value="News">News</option>
										<option value="Events">Events &amp; Training</option>
									</select>
								</div>
								<div class="col-md-6">
							       <div id="dropBox" class="apm-text-field"><p>Select background image on app</p></div>
							           <input type="file" name="fileInput" id="fileInput" />
					            </div>
								<div class="col-md-12"><textarea class="form-control apm-text-field" placeholder="Main Context/News" id="txtnewscontextinfor" rows="4"></textarea></div>							
								<div class="col-md-6">
								<button type="button" class="btn btn-primary btn-block" id="createNewsBtn" onClick="createNewsEvents()">Create</button>
								</div>
								</div>
								</div>
							</div>
						</div>
					</div>
					<!--Location Inquiry-->
					<div class="row" id="eventsListPanel">
						
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading search-header">
									<div class="col-md-3">News &amp; Events</div>
									<div class="col-md-1"><i class="fa fa-plus-circle" onclick="loadCreateNewsEvents()" style="font-size: 36px;color: #F58A22;cursor: pointer;"></i></span></div>
									<div class="col-md-3">
									<select class="form-control" id="catevents">
									    <option value="404" selected>--Filter By Category--</option>
									    <option value="Simon">Simon Sabela Film &amp; TV Awards</option>
										<option value="News">News</option>
										<option value="Events">Events &amp; Training</option>
									</select>
									</div>
									<div class="col-md-3">
									<select class="form-control" id="astatevents">
									    <option value="404" selected>--Filter By Status--</option>
										<option value="1">Active/Showing on App</option>
										<option value="0">Inactive/Showcase Request</option>
									</select>
									</div>
									<div class="col-md-2 search-header2">
									<div class="input-group apm-text-field">
										<span class="input-group-addon"><i class="fa fa-search"></i></span>
										<input type='text' class="form-control apm-text-field" placeholder="Search" id="searchEvents" />
									</div> 
								   </div> 
								</div>
								<div class="panel-body">
								<div class="row">
					             <table class="table" id="eventsTable">
										<thead>
											<tr>
												<th>#</th>
												<th>Headline</th>
												<th>Subtitle</th>
												<th>Context</th>
												<th>Status</th>
												<th>Posted</th>
												<th>Toggle</th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
											</tr>
										</thead>
										<tbody id="eventsListDiv">
											
										</tbody>
									</table>			             
								</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row" id="editEventPicPanel">
                      <div class="col-md-10">
							 <div id="edropBox"><p>Select image to upload</p></div>
							 <input type="file" name="fileInput" id="efileInput" />
					   </div>
					  <div class="col-md-2">
							<input type="button" name="" class="btn btn-primary" value="SAVE" style="width: 100%; margin-top: 15px;" id="apply" onclick="saveEditEventImage()" />
		             </div>
                   </div>

                   <!--LOCATIONS HERE START-->
                   <div class="row" id="editLocationPanel">
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Update Location</h3>
								</div>
								<div class="panel-body">
								<div class="row">
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Place" id="txteditlocationheadlines"></div>
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Subtitle" id="txteditlocationsubtitle"></div>
								<div class="col-md-4"><input type="text" class="form-control apm-text-field" placeholder="Latitude" id="txteditlocationlat"></div>
								<div class="col-md-4"><input type="text" class="form-control apm-text-field" placeholder="Longitude" id="txteditlocationlng"></div>
								<div class="col-md-4">
									<select class="form-control" id="txteditlocationcategory">
									    <option value="African Bush" selected>--Select Option--</option>
										<option value="African Bush">African Bush</option>
										<option value="Beach">Beach</option>
										<option value="Mountains">Mountains</option>
										<option value="Rural">Rural</option>
										<option value="Tropical Forest">Tropical Forest</option>
										<option value="Urban">Urban</option>
									</select>
								</div>
								<div class="col-md-12"><textarea class="form-control apm-text-field" placeholder="Main Context" id="txteditlocationcontextinfor" rows="4"></textarea></div>							
								<div class="col-md-6">
								<button type="button" class="btn btn-primary btn-block" id="editLocationBtn" onClick="updateLocationLocation()">Update</button>
								</div>
								</div>
								</div>
							</div>
						</div>
					</div>
                
                   	<div class="row" id="createLocationPanel">
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Add Location</h3>
								</div>
								<div class="panel-body">
								<div class="row">
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Place" id="txtlocationheadlines"></div>
								<div class="col-md-6"><input type="text" class="form-control apm-text-field" placeholder="Subtitle" id="txtlocationsubtitle"></div>
								<div class="col-md-4"><input type="text" class="form-control apm-text-field" placeholder="Latitude" id="txtlocationlat"></div>
								<div class="col-md-4"><input type="text" class="form-control apm-text-field" placeholder="Longitude" id="txtlocationlng"></div>
								<div class="col-md-4">
									<select class="form-control" id="txtlocationcategory">
									    <option value="African Bush" selected>--Select Option--</option>
										<option value="African Bush">African Bush</option>
										<option value="Beach">Beach</option>
										<option value="Mountains">Mountains</option>
										<option value="Rural">Rural</option>
										<option value="Tropical Forest">Tropical Forest</option>
										<option value="Urban">Urban</option>
									</select>
								</div>
								<div class="col-md-6">
							       <div id="cldropBox" class="apm-text-field"><p>Select background image on app</p></div>
							           <input type="file" name="fileInput" id="clfileInput" />
					            </div>
								<div class="col-md-12"><textarea class="form-control apm-text-field" placeholder="Main Context" id="txtlocationcontextinfor" rows="4"></textarea></div>							
								<div class="col-md-6">
								<button type="button" class="btn btn-primary btn-block" id="createLocationBtn" onClick="createLocationLocation()">Create</button>
								</div>
								</div>
								</div>
							</div>
						</div>
					</div>
					<!--Location Inquiry-->
					<div class="row" id="locationListPanel">
						
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading search-header">
									<div class="col-md-3">Locations</div>
									<div class="col-md-1"><i class="fa fa-plus-circle" onclick="loadCreateLocationLocation()" style="font-size: 36px;color: #F58A22;cursor: pointer;"></i></span></div>
									<div class="col-md-3">
									<select class="form-control" id="catlocation">
									    <option value="404" selected>--Select Option--</option>
										<option value="African Bush">African Bush</option>
										<option value="Beach">Beach</option>
										<option value="Mountains">Mountains</option>
										<option value="Rural">Rural</option>
										<option value="Tropical Forest">Tropical Forest</option>
										<option value="Urban">Urban</option>
									</select>
									</div>
									<div class="col-md-3">
									<select class="form-control" id="astatlocation">
									    <option value="404" selected>--Filter By Status--</option>
										<option value="1">Active/Showing on App</option>
										<option value="0">Inactive/Showcase Request</option>
									</select>
									</div>
									<div class="col-md-2 search-header2">
									<div class="input-group apm-text-field">
										<span class="input-group-addon"><i class="fa fa-search"></i></span>
										<input type='text' class="form-control apm-text-field" placeholder="Search" id="searchLocation" />
									</div> 
								   </div> 
								</div>
								<div class="panel-body">
								<div class="row">
					             <table class="table" id="locationTable">
										<thead>
											<tr>
												<th>#</th>
												<th>Place</th>
												<th>Subtitle</th>
												<th>Context</th>
												<th>Status</th>
												<th>Posted</th>
												<th>Toggle</th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
											</tr>
										</thead>
										<tbody id="locationListDiv">
											
										</tbody>
									</table>			             
								</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row" id="editLocationPicPanel">
                      <div class="col-md-10">
							 <div id="eldropBox"><p>Select image to upload</p></div>
							 <input type="file" name="fileInput" id="elfileInput" />
					   </div>
					  <div class="col-md-2">
							<input type="button" name="" class="btn btn-primary" value="SAVE" style="width: 100%; margin-top: 15px;" id="apply" onclick="saveEditLocationImage()" />
		             </div>
                   </div>

                   <!--LOCATIONS HERE END -->

						<!--Location Inquiry-->
					<div class="row" id="locationInquiryPanel">
						
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading search-header">
									<div class="col-md-9">Location Inquiries</div>
									<div class="col-md-3 search-header2">
									<div class="input-group apm-text-field">
										<span class="input-group-addon"><i class="fa fa-search"></i></span>
										<input type='text' class="form-control apm-text-field" placeholder="Search" id="searchLocationInquire" />
									</div> 
								   </div> 
								</div>
								<div class="panel-body">
								<div class="row">
					             <table class="table" id="clientsTable">
										<thead>
											<tr>
												<th>#</th>
												<th>Location</th>
												<th>Company</th>
												<th>Name</th>
												<th>Email</th>
												<th>Additional Info</th>
												<th>Date</th>
												<th></th>
											</tr>
										</thead>
										<tbody id="locInquiryListDiv">
											
										</tbody>
									</table>			             
								</div>
								</div>
							</div>
						</div>
					</div>

						<!--Location Inquiry-->
					<div class="row" id="facilityInquiryPanel">
						
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading search-header">
									<div class="col-md-9">Facility Inquiries</div>
									<div class="col-md-3 search-header2">
									<div class="input-group apm-text-field">
										<span class="input-group-addon"><i class="fa fa-search"></i></span>
										<input type='text' class="form-control apm-text-field" placeholder="Search" id="searchFacilityInquire" />
									</div> 
								   </div> 
								</div>
								<div class="panel-body">
								<div class="row">
					             <table class="table" id="facTable">
										<thead>
											<tr>
												<th>#</th>
												<th>Company</th>
												<th>Name</th>
												<th>ID#</th>
												<th>Email</th>
												<th>Phone</th>
												<th>Inquiry Info</th>
												<th>Date</th>
												<th></th>
											</tr>
										</thead>
										<tbody id="facInquiryListDiv">
											
										</tbody>
									</table>			             
								</div>
								</div>
							</div>
						</div>
					</div>

								<!--Location Inquiry-->
					<div class="row" id="bursaryInquiryPanel">			
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading search-header">
									<div class="col-md-9">Bursary Application</div>
									<div class="col-md-3 search-header2">
									<div class="input-group apm-text-field">
										<span class="input-group-addon"><i class="fa fa-search"></i></span>
										<input type='text' class="form-control apm-text-field" placeholder="Search" id="searchBursary" />
									</div> 
								   </div> 
								</div>
								<div class="panel-body">
								<div class="row">
					             <table class="table" id="busTable">
										<thead>
											<tr>
												<th>#</th>
												<th>Name</th>
												<th>Gender</th>	
												<th>Email</th>
												<th>Age</th>
												<th>Study Field</th>
												<th>Year</th>
												<th>Bursary Option</th>
												<th>Other Info</th>
												<th>Submitted</th>
												<th></th>
											</tr>
										</thead>
										<tbody id="busInquiryListDiv">
											
										</tbody>
									</table>			             
								</div>
								</div>
							</div>
						</div>
					</div>


								<!--Location Inquiry-->
					<div class="row" id="fundingInquiryPanel">
						
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading search-header">
									<div class="col-md-9">Funding Inquiry</div>
									<div class="col-md-3 search-header2">
									<div class="input-group apm-text-field">
										<span class="input-group-addon"><i class="fa fa-search"></i></span>
										<input type='text' class="form-control apm-text-field" placeholder="Search" id="searchFunding" />
									</div> 
								   </div> 
								</div>
								<div class="panel-body">
								<div class="row">
					             <table class="table" id="funTable">
										<thead>
											<tr>
												<th>#</th>
												<th>Funding Type</th>
												<th>Company</th>
												<th>Name</th>
												<th>Email</th>					<th>Other Info</th>
												<th>Date</th>
												<th></th>
											</tr>
										</thead>
										<tbody id="funInquiryListDiv">
											
										</tbody>
									</table>			             
								</div>
								</div>
							</div>
						</div>
					</div>

								<!--Location Inquiry-->
					<div class="row" id="crewInquiryPanel">
						
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading search-header">
									<div class="col-md-9">Crew Inquiry</div>
									<div class="col-md-3 search-header2">
									<div class="input-group apm-text-field">
										<span class="input-group-addon"><i class="fa fa-search"></i></span>
										<input type='text' class="form-control apm-text-field" placeholder="Search" id="searchCrew" />
									</div> 
								   </div> 
								</div>
								<div class="panel-body">
								<div class="row">
					             <table class="table" id="crewTable">
										<thead>
											<tr>
												<th>#</th>
												<th>Category</th>
												<th>Company</th>
												<th>Name</th>
												<th>Email</th>
												<th>Other Info</th>
												<th>Date</th>
												<th></th>
											</tr>
										</thead>
										<tbody id="crewInquiryListDiv">
											
										</tbody>
									</table>			             
								</div>
								</div>
							</div>
						</div>
					</div>


					<div class="row" id="trainingInquiryPanel">
						<div class="col-md-12">
							<!-- BUTTONS -->
							<div class="panel">
								<div class="panel-heading search-header">
									<div class="col-md-9">Traning Inquiry</div>
									<div class="col-md-3 search-header2">
									<div class="input-group apm-text-field">
										<span class="input-group-addon"><i class="fa fa-search"></i></span>
										<input type='text' class="form-control apm-text-field" placeholder="Search" id="searchTraining" />
									</div> 
								   </div> 
								</div>
								<div class="panel-body">
								<div class="row">
					             <table class="table" id="trainTable">
										<thead>
											<tr>
												<th>#</th>
												<th>Training Type</th>
												<th>Company</th>
												<th>Name</th>
												<th>Email</th>					<th>Other Info</th>
												<th>Date</th>
												<th></th>
											</tr>
										</thead>
										<tbody id="trainingInquiryListDiv">
											
										</tbody>
									</table>			             
								</div>
								</div>
							</div>
						</div>
					</div>
				
				
					<div class="row dashboardPanel">
                                       	<div class="row">
						<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
								<div class="row">
									<div class="col-md-12"><h3 class="panel-title" id="srtitle">Logged</h3></div>															
								</div>
							</div>
								<div class="panel-body">
								<div class="row">
				            <div class="col-md-12">
					            <table class="table" width="60%" align="center" style="align-self: center !important">
									<tbody>
									 <tr>
								      <td >Name</td>
	                                  <td id="loggedName"><?php echo $_SESSION['nam']; ?></td>
							        </tr>
							        <tr>
								      <td >Email</td>
	                                  <td >
	                                  <?php echo $_SESSION['email']; ?></td>
							        </tr>
							        <tr>
								      <td >Phone</td>
	                                  <td ><?php echo $_SESSION['cell']; ?></td>
							        </tr>									 
									</tbody>
								</table>
								<strong>*If this is not you please contact admin*</strong> &nbsp;
								<strong>NB: All transactions are tracked</strong>
								</div>
								</div>
								</div>			
						</div>
					</div>
						</div>
					</div>
					
				</div>
			</div>
			<!-- END MAIN CONTENT -->	
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	
	<script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>  
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/js/klorofil.min.js"></script>
	<!-- Bootstrap Date-Picker Plugin -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <script src="libs/js/alertify.js"></script>  

	<!-- Custom Javascript -->
	<script src="js/users.js"></script>	
	<script src="js/clients.js"></script>	
	<script src="js/locationinquire.js"></script>	
	<script src="js/fundinginquire.js"></script>	
	<script src="js/facilityinquire.js"></script>	
	<script src="js/bursaryinquire.js"></script>
	<script src="js/traininginquire.js"></script>
	<script src="js/crewinquire.js"></script>
    <script src="js/showcase.js"></script>
	<script src="js/employees.js"></script>
	<script src="js/events.js"></script>
	<script src="js/social.js"></script>
	<script src="js/facility.js"></script>
	<script src="js/location.js"></script>
	<script src="js/common.js"></script>
</body>
</html>