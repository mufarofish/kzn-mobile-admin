<?php
header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
header('Content-type: application/json');
include '../config/DB.php';
include 'CypherController.php';
include 'ObjectValueController.php';
$db = new DB();
$tblName = 'app_users';
$type = $_GET['rtype'];
if(isset($type) && !empty($type)){
    switch($type){
        
        case "activate":  
            $userData = array(
                'status' => 1
            );
            $key=decrypt(str_replace(' ','+', $_GET['key']));
             $condition = array('id' => $key);
            $filterArray = array('where' => $condition);
            $userInfor = $db->getRows($tblName, $filterArray);
            $update = $db->update($tblName,$userData,$condition);
            if($update){
                //Send Email to Dude
                $from="donotreply@kzn-app.com";
                 $email = get_value($userInfor, 'email', '');
                 $name = get_value($userInfor, 'name', '');
                 $subject= "Welcome to KZN";
                 $message = "Hi..Your KZN app account has been activated. You can login your app. Thank you"."\r\n";
                 $header = "From:" . $from." \r\n";
                 $header .= "MIME-Version: 1.0\r\n";
                 $header .= "Content-type: text/html\r\n";       
                 $retval = mail ($email,$subject,$message,$header);
                
                  echo 'Account Activated';
            }else{
                  echo 'Activation Failed';
            }        
            break;
        default:
            echo 'ERROR CONTACT ADMIN';
    }
}