<?php
header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
header('Content-type: application/json');
include '../config/DB.php';
include 'CypherController.php';
include 'ObjectValueController.php';
$db = new DB();
$tblName = 'guest_users';
$type = $_GET['rtype'];
$_POST = json_decode(file_get_contents("php://input"), true);
if(isset($type) && !empty($type)){
    switch($type){
      case "view":
            $records = $db->getRows($tblName);
            if($records){
                $data['records'] = $db->getRows($tblName);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;

        case "viewbyid":
            $orderby='created DESC';
            if(isset($_GET['id'])){
                $where=array('id'=>$_GET['id']);   
            }
            $filterArray = array('where' => $where, 'order_by'=>$orderby);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
			
        case "add":         
              $userData = array(
                'name' => $_POST['name'],
                'email' => $_POST['email'],
				        'phone' => $_POST['phone'],
				        'status' => 1,
                'company' => $_POST['company'],
                'visualimpared' => 'false',
                'newsletter' => 'false',
				        'pass' => encrypt($_POST['pass'])           
              );
              $orderby='created DESC';
              $where=array('email'=>$_POST['email']);   
              $filterArray = array('where' => $where, 'order_by'=>$orderby);
              $records = $db->getRows($tblName, $filterArray);
              if($_POST['pass']!=$_POST['confirmpass']){
                $data['stat'] = 'ERR';
                $data['msg'] = 'Failed! Passwords not matching. ';
              }
              else
              if($records){
                $data['stat'] = 'ERR';
                $data['msg'] = 'Email already registered. Try another email.';
              }
              else{
                if($userData){
                $insert = $db->insert($tblName,$userData);
                  if($insert){
                    //$data['data'] = $insert;
                    $name = $_POST['name'];
                    $email = $_POST['email'];
                    $id = get_value($insert, 'id', '');
                    $company = $_POST['company'];
                    $phone =  $_POST['phone'];
                    $data['stat'] = 'OK';
                    $data['name'] = $name;
                    $data['email'] = $email;
                    $data['company'] = $company;
                    $data['phone'] = $phone;
                    $data['visualimpared'] = 'false';
                    $data['newsletter'] = 'false';
                    $data['id'] = $id;
                   
                    
                     /*$idenc = encrypt($id);  
                     $url = $db->baseURL();  
                     //Send Email to Dude
                     $from="donotreply@kwazulunatalfilm.co.za";
                     $appLink="https://".$url."/kznapp/mobile/ActivateAccountController.php?rtype=activate&key=".$idenc;
                     $subject= $_POST['name']. " Welcome to KZN";
                     $message = "Hi  ".$_POST['name'] .  ". You have been registered to KZN mobile application. Please click this link activate your account:  " .$appLink. ". Use your login email and password to use the app. Your password is : [".$_POST['pass']."]. Thank you! [DO NOT REPLY THIS EMAIL]"."\r\n";
                     $header = "From:" . $from." \r\n";
                     $header .= "MIME-Version: 1.0\r\n";
                     $header .= "Content-type: text/html\r\n";       
                     $retval = mail($email,$subject,$message,$header);
                     if($retval){
                          $data['msg'] = 'Account created successfully. Check email to activate!'; 
                     }
                     else{
                           $data['msg'] = 'Email sending failed'; 
                     }*/
                     $data['msg'] = 'Account created successfully. Check email to activate!'; 
                    
                  }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                  }
                }else{
                 $data['stat'] = 'ERR';
                 $data['msg'] = 'Some problem occurred, please try again.';
                }

            }
			  
            echo json_encode($data);
            break;

         case "reset":         
              $userData = array(
                'email' => $_POST['username']        
              );
              $where=array('email'=>$_POST['username']);   
              $filterArray = array('where' => $where);
              $records = $db->getRows($tblName, $filterArray);
              if($records){
                     $name = $records[0]['name'];
                     $pass = $records[0]['pass'];
                     $email = $records[0]['email'];
                     $found = decrypt($pass); 
                     $data['stat'] = 'OK';
                     $data['msg'] = "Check email for recovered password";  
                     
                     //Send Email to Dude
                     $from="donotreply@kwazulunatalfilm.co.za";
                     $subject= "Password Recovery KZN APP";
                     $message = "Hi  ".$name.  ". We have successfully recovered your password. Your password is : ".$found.". Thank you! [DO NOT REPLY THIS EMAIL]"."\r\n";
                     $header = "From:" . $from." \r\n";
                     $header .= "MIME-Version: 1.0\r\n";
                     $header .= "Content-type: text/html\r\n";       
                     $retval = mail ( $email,$subject,$message,$header);
                    
              }
              else{         
                 $data['stat'] = 'ERR';
                 $data['msg'] = 'Email not found';              
              }
        
            echo json_encode($data);
            break;

        case "login":         
              $userData = array(
                'email' => $_POST['username'],
                'pass' => encrypt($_POST['password'])  
              );
              $orderby='created DESC';
              $where=array('email'=>$_POST['username'], 'pass'=>encrypt($_POST['password']));   
              $filterArray = array('where' => $where, 'order_by'=>$orderby);
              $records = $db->getRows($tblName, $filterArray);
              if($records){
                 $where=array('email'=>$_POST['username'], 'pass'=>encrypt($_POST['password']), 'status'=>1); 
                 $filterArray = array('where' => $where, 'order_by'=>$orderby);
                 $astat = $db->getRows($tblName, $filterArray);

                 if($astat){
                  $name = $records[0]['name'];
                  $id = $records[0]['id'];
                  $email = $records[0]['email'];
                  $company = $records[0]['company'];
                  $phone = $records[0]['phone'];
                  $visualimpared = $records[0]['visualimpared'];
                  $newsletter = $records[0]['newsletter'];
                  $data['name'] = $name;
                  $data['email'] = $email;
                  $data['phone'] = $phone;
                  $data['id'] = $id;
                  $data['company'] = $company;
                  $data['visualimpared'] = $visualimpared;
                  $data['newsletter'] = $newsletter;
                  $data['stat'] = 'OK';
                  $data['msg'] = 'Login Success';
                 }
                 else{
                   $data['stat'] = 'ERR';
                   $data['msg'] = 'Activate your account. Check your email';  
                 }
                    
              }
              else{
                   $data['stat'] = 'ERR';
                   $data['msg'] = 'Invalid login details';
              }              
              echo json_encode($data);
              break;
        case "edit":          
            $userData = array(
                'name' => $_POST['name'],
                'email' => $_POST['email'],
                'company' => $_POST['company'],
                'phone' => $_POST['phone'],
                'visualimpared' => $_POST['visualimpared'],
                'newsletter' => $_POST['newsletter']
            );
			      if($userData){
                $condition = array('id' => $_GET['id']);
                $update = $db->update($tblName,$userData,$condition);
                if($update){
                    $name = $_POST['name'];
                    $email = $_POST['email'];
                    $company = $_POST['company'];
                    $phone =  $_POST['phone'];
                    $visualimpared =  $_POST['visualimpared'];
                    $newsletter =  $_POST['newsletter'];
                    $id =  $_GET['id'];
                    $data['name'] = $name;
                    $data['email'] = $email;
                    $data['company'] = $company;
                    $data['phone'] = $phone;
                    $data['visualimpared'] = $visualimpared;
                    $data['newsletter'] = $newsletter;
                    $data['id'] = $id;
                    $data['stat'] = 'OK';
                    $data['msg'] = 'Profile updated successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, in creation please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, in accessing please try again.';
            }
            echo json_encode($data);
            break;
         case "changepass":          
            $userData = array(
                    'pass' => encrypt($_POST['pass'])
            );
            $where=array('id'=>$_GET['id'], 'pass'=>encrypt($_POST['oldpass']));   
            $filterArray = array('where' => $where);
            $records = $db->getRows($tblName, $filterArray);
            if($_POST['pass']!=$_POST['confirmpass']){
                $data['stat'] = 'ERR';
                $data['msg'] = 'Failed! Passwords not matching. ';
            }
            else
            if($records){
                if($userData){
                    $condition = array('id' => $_GET['id']);
                    $update = $db->update($tblName,$userData,$condition);
                    if($update){
                        $data['stat'] = 'OK';
                        $data['msg'] = 'Password updated successfully.';
                    }else{
                        $data['stat'] = 'ERR';
                        $data['msg'] = 'Some problem occurred, please try again.';
                    }
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }
            else{
              $data['stat'] = 'ERR';
              $data['msg'] = 'Failed.. Invalid old password';
            }
            echo json_encode($data);
            break;
        case "delete":           
              $condition = array('id' => $_POST['id']);
			  if($condition){
                $delete = $db->delete($tblName,$condition);
                if($delete){
                    $data['stat'] = 'OK';
                    $data['msg'] = 'User data has been deleted successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
        default:
            echo '{"status":"INVALID"}';
    }
}