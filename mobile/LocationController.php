<?php
header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
header('Content-type: application/json');
include '../config/DB.php';
include 'CypherController.php';
include 'ObjectValueController.php';
$db = new DB();
$tblName = 'locations';
$type = $_GET['rtype'];
$_POST = json_decode(file_get_contents("php://input"), true);
if(isset($type) && !empty($type)){
    switch($type){
        case "view":
			$orderby='created DESC';
			$filterArray = array('order_by'=>$orderby);
            $records = $db->getRowsLocationAll($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRowsLocationAll($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
		case "viewbyid":
			$orderby='created DESC';
			$where=array('status'=>1);	
			$filterArray = array('where' => $where, 'order_by'=>$orderby);
            $records = $db->getRowsLocationId($tblName, $filterArray);
            if($records){
                $data = $db->getRowsLocationId($tblName, $filterArray);
               
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
        default:
            echo '{"status":"INVALID"}';
    }
}