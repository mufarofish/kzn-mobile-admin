<?php
session_start();
header('Content-type: application/json');
include '../config/DB.php';
$db = new DB();
$tblName = 'docs';
$type=$_POST['rtype'];
if(isset($type) && !empty($type)){
    switch($type){
        case "view":
            $orderby='created DESC';
			$filterArray = array('order_by'=>$orderby);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
		 case "viewbyid":
            $_SESSION['client_id']=$_POST['client_id'];
			$where=array('client_id'=>$_POST['client_id']);	
			$filterArray = array('where' => $where);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
       
          default:
            echo '{"status":"INVALID"}';
    }
}