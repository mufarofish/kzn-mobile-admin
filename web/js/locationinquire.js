// Users javascript

function deleteLocationInquire(value, object) {
	var rtype="delete";
	var a = confirm("Are you sure to delete this record?");
     if(a) {
$.post("controllers/LocationInquireController.php",{
       id: value,
	   rtype: rtype
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetLocationInquiries();
       }    
)
}
}

function ajaxGetLocationInquiries(){
 var rtype="view";
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype
    },
    url: "controllers/LocationInquireController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
	  var count=0;
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			  count++
        responsesHTML += "<tr>";
	      responsesHTML += "<td>" + count + "</td>";
         responsesHTML += "<td>" + JSONObject[key]["cat"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["company"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["name"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["email"] + "</td>";
	      responsesHTML += "<td>" + JSONObject[key]["otherinfor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";
	      responsesHTML += "<td onClick='deleteLocationInquire("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete User'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#locInquiryListDiv").html(responsesHTML);
    }
  });
}

function ajaxSearchLocationInquiries(filterText){
   var rtype="search";
   var filterT=filterText;
   $.ajax({
    type: "POST",
    data: {
    "rtype": rtype,
	  "filterText": filterT
    },
    url: "controllers/LocationInquireController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
	  var count=0;
      // Loop through Object and create peopleHTML
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			  count++
        responsesHTML += "<tr>";
	      responsesHTML += "<td>" + count + "</td>";
         responsesHTML += "<td>" + JSONObject[key]["cat"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["company"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["name"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["email"] + "</td>";
	      responsesHTML += "<td>" + JSONObject[key]["otherinfor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";
	      responsesHTML += "<td onClick='deleteLocationInquire("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete User'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#locInquiryListDiv").html(responsesHTML);
    }
  });
     //End of ajax
}
$('#searchLocationInquire').keyup(function (e) {
	var selected=$('#searchLocationInquire').val();
	 ajaxSearchLocationInquiries(selected);
})