// Users javascript

function deleteEvent(value, object) {
	var rtype="delete";
	var a = confirm("Are you sure to delete this record?");
     if(a) {
$.post("controllers/EventsController.php",{
       id: value,
	   rtype: rtype
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetEvents();
       }    
)
}
}

function editEvent(id){
  var rtype="viewbyid";
   $.ajax({
    type: "POST",
    data: {
    "rtype": rtype,
    "id": id
    },
    url: "controllers/EventsController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
    var count=0;
      // Loop through Object and create peopleHTML
    var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
       if (JSONObject.hasOwnProperty(key)) {
      count++ 
      //Put car details
        $('#txteditnewsheadlines').val(JSONObject[key]["infor"]);
        $('#txteditnewssubtitle').val(JSONObject[key]["moreinfor"]);
        $('textarea#txteditnewscontextinfor').val(JSONObject[key]["contextinfor"]);
       }         
      }
      loadViewEditEvents();    
    }
  });
}

function changeEventStatus(idToUpdate, setStatusTo, refreshStatus) {
var rtype="editstatus";
$.post("controllers/EventsController.php",{
       id: idToUpdate,
       rtype: rtype,
       setto: setStatusTo
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetEventsByStatus(refreshStatus); 
      }    
)
}

function changeEventFeatureStatus(idToUpdate, setStatusTo, refreshStatus) {
var rtype="editfstatus";
$.post("controllers/EventsController.php",{
       id: idToUpdate,
       rtype: rtype,
       setto: setStatusTo
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetEvents();
      }    
)
}

function editPicEvent(id){
    var rtype = 'savesession';
    $.post("controllers/EventsController.php",{
       id: id,
       rtype: rtype
       },
       function(response,status){ // Required Callback Function     
        $('#editEventPicPanel').hide(); 
        $('#editEventPicPanel').show("slow");
      }    
)
    $('html,body,window').animate({scrollTop: $("#editEventPicPanel").offset().top},'slow');
}

function saveEditEventImage(){
   var rtype = 'saveeventimage';
    $.post("controllers/EventsController.php",{
       rtype: rtype
       },
       function(response,status){ // Required Callback Function     
        $('#editEventPicPanel').hide("slow"); 
      }    
)
}


function ajaxGetEvents(){
 var rtype="view";
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype
    },
    url: "controllers/EventsController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
	  var count=0;
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			  count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["infor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["moreinfor"] + "</td>";
        var cont = JSONObject[key]["contextinfor"];
        var text = cont.substr(0, 20) + "...";
        responsesHTML += "<td>"+ text +"</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";   
        if(JSONObject[key]["feature"]==0){
         responsesHTML += "<td onClick='changeEventFeatureStatus("+JSONObject[key]["id"]+", 1, 0)'><span class='badge badge-secondary' style='cursor:pointer;'>Not Featured</span></td>";
        }
        if(JSONObject[key]["feature"]==1){
         responsesHTML += "<td onClick='changeEventFeatureStatus("+JSONObject[key]["id"]+", 0, 1)'><span class='badge badge-success' style='background-color:#28A745;cursor:pointer;'>Featured</span></td>";
        }
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeEventStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeEventStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        var link = getImageLink();
        responsesHTML += "<td><a href= "+link+JSONObject[key]["image"] +" target='_blank'>View Image</a></td>";
        responsesHTML += "<td onClick='editPicEvent("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Image</td>";
        responsesHTML += "<td onClick='editEvent("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Text</td>";
        responsesHTML += "<td onClick='deleteEvent("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#eventsListDiv").html(responsesHTML);
    }
  });
}

function ajaxGetEventsByStatus(status){
   var rtype="viewbystatus";
   if(status==404){
     rtype = "view";
   }
   if(status==0){
     rtype = "viewbystatus";
   }
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype,
      "status": status
    },
    url: "controllers/EventsController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
    var count=0;
    var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
        count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["infor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["moreinfor"] + "</td>";
        var cont = JSONObject[key]["contextinfor"];
        var text = cont.substr(0, 20) + "...";
        responsesHTML += "<td>"+ text +"</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";   
        if(JSONObject[key]["feature"]==0){
         responsesHTML += "<td onClick='changeEventFeatureStatus("+JSONObject[key]["id"]+", 1, 0)'><span class='badge badge-secondary' style='cursor:pointer;'>Not Featured</span></td>";
        }
        if(JSONObject[key]["feature"]==1){
         responsesHTML += "<td onClick='changeEventFeatureStatus("+JSONObject[key]["id"]+", 0, 1)'><span class='badge badge-success' style='background-color:#28A745;cursor:pointer;'>Featured</span></td>";
        }
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeEventStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeEventStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        var link = getImageLink();
        responsesHTML += "<td><a href= "+link+JSONObject[key]["image"] +" target='_blank'>View Image</a></td>";
        responsesHTML += "<td onClick='editPicEvent("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Image</td>";
        responsesHTML += "<td onClick='editEvent("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Text</td>";
        responsesHTML += "<td onClick='deleteEvent("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#eventsListDiv").html(responsesHTML);
    }
  });
}

function ajaxGetEventsByCat(cat){
   var rtype="viewbycat";
   if(cat==404){
     rtype = "view";
   }
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype,
      "cat": cat
    },
    url: "controllers/EventsController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
    var count=0;
    var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
        count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["infor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["moreinfor"] + "</td>";
        var cont = JSONObject[key]["contextinfor"];
        var text = cont.substr(0, 20) + "...";
        responsesHTML += "<td>"+ text +"</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";   
        if(JSONObject[key]["feature"]==0){
         responsesHTML += "<td onClick='changeEventFeatureStatus("+JSONObject[key]["id"]+", 1, 0)'><span class='badge badge-secondary' style='cursor:pointer;'>Not Featured</span></td>";
        }
        if(JSONObject[key]["feature"]==1){
         responsesHTML += "<td onClick='changeEventFeatureStatus("+JSONObject[key]["id"]+", 0, 1)'><span class='badge badge-success' style='background-color:#28A745;cursor:pointer;'>Featured</span></td>";
        }
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeEventStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeEventStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        var link = getImageLink();
        responsesHTML += "<td><a href= "+link+JSONObject[key]["image"] +" target='_blank'>View Image</a></td>";
        responsesHTML += "<td onClick='editPicEvent("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Image</td>";
        responsesHTML += "<td onClick='editEvent("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Text</td>";
        responsesHTML += "<td onClick='deleteEvent("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#eventsListDiv").html(responsesHTML);
    }
  });
}


function ajaxSearchEvent(filterText){
   var rtype="search";
   var filterT=filterText;
   $.ajax({
    type: "POST",
    data: {
    "rtype": rtype,
	  "filterText": filterT
    },
    url: "controllers/EventsController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
	  var count=0;
      // Loop through Object and create peopleHTML
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			  count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["infor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["moreinfor"] + "</td>";
        var cont = JSONObject[key]["contextinfor"];
        var text = cont.substr(0, 20) + "...";
        responsesHTML += "<td>"+ text +"</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";   
        if(JSONObject[key]["feature"]==0){
         responsesHTML += "<td onClick='changeEventFeatureStatus("+JSONObject[key]["id"]+", 1, 0)'><span class='badge badge-secondary' style='cursor:pointer;'>Not Featured</span></td>";
        }
        if(JSONObject[key]["feature"]==1){
         responsesHTML += "<td onClick='changeEventFeatureStatus("+JSONObject[key]["id"]+", 0, 1)'><span class='badge badge-success' style='background-color:#28A745;cursor:pointer;'>Featured</span></td>";
        }
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeEventStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeEventStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        var link = getImageLink();
        responsesHTML += "<td><a href= "+link+JSONObject[key]["image"] +" target='_blank'>View Image</a></td>";
        responsesHTML += "<td onClick='editPicEvent("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Image</td>";
        responsesHTML += "<td onClick='editEvent("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Text</td>";
        responsesHTML += "<td onClick='deleteEvent("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#eventsListDiv").html(responsesHTML);
    }
  });
     //End of ajax
}
$('#searchEvents').keyup(function (e) {
	var selected=$('#searchEvents').val();
	 ajaxSearchEvent(selected);
})
 $("#astatevents").change(function () {
	   var selected=$('#astatevents').val();
     ajaxGetEventsByStatus(selected);
 });
$("#catevents").change(function () {
     var selected=$('#catevents').val();
     ajaxGetEventsByCat(selected);
 });
//Saving and Updating
function createNewsEvents() {
      var headlines=$('#txtnewsheadlines').val();
      var subtitle=$('#txtnewssubtitle').val();
      var contextinfor=$('#txtnewscontextinfor').val();
      var category=$('#txtnewscategory').val();
      var rtype="add";  
      if ((/^\s*$/).test(headlines)) {
         Materialize.toast("Enter headlines", 3000);      
             }
      else
      if ((/^\s*$/).test(contextinfor)) {
         Materialize.toast("Enter context information.", 3000);           
      } else {
       $.post("controllers/EventsController.php",{
       rtype: rtype,
       headline: headlines,
       category: category,
       subtitle:subtitle,
       contextinfor:contextinfor
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       if(stat=="OK"){
       Materialize.toast("News/Events posted successfully", 3000, '',function(){  clearFields(); });  
       }
       else{
       Materialize.toast("Error! " + msg, 6000);      
       }

       }    
       )};
}

function updateNewsEvents() {
      var headlines=$('#txteditnewsheadlines').val();
      var subtitle=$('#txteditnewssubtitle').val();
      var contextinfor=$('#txteditnewscontextinfor').val();
      var category=$('#txteditnewscategory').val();
      var rtype="update";  
      if ((/^\s*$/).test(headlines)) {
         Materialize.toast("Enter headlines", 3000);      
             }
      else
      if ((/^\s*$/).test(contextinfor)) {
         Materialize.toast("Enter context information.", 3000);           
      } else {
       $.post("controllers/EventsController.php",{
       rtype: rtype,
       headline: headlines,
       category: category,
       subtitle:subtitle,
       contextinfor:contextinfor
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       if(stat=="OK"){
       Materialize.toast("News/Events updated successfully", 3000, '',function(){  clearFields(); });  
       }
       else{
       Materialize.toast("Error! " + msg, 6000);      
       }
       }    
     )};
}