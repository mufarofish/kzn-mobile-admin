// Users javascript

function deleteCrewInquire(value, object) {
	var rtype="delete";
	var a = confirm("Are you sure to delete this record?");
     if(a) {
$.post("controllers/CrewInquireController.php",{
       id: value,
	   rtype: rtype
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetCrewInquiries();
       }    
)
}
}

function ajaxGetCrewInquiries(){
 var rtype="view";
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype
    },
    url: "controllers/CrewInquireController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
	  var count=0;
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			  count++
        responsesHTML += "<tr>";
	      responsesHTML += "<td>" + count + "</td>";
         responsesHTML += "<td>" + JSONObject[key]["ftype"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["company"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["name"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["email"] + "</td>";
	      responsesHTML += "<td>" + JSONObject[key]["otherinfor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";
	      responsesHTML += "<td onClick='deleteCrewInquire("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Infor'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#crewInquiryListDiv").html(responsesHTML);
    }
  });
}

function ajaxSearchCrewInquiries(filterText){
   var rtype="search";
   var filterT=filterText;
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype,
      "filterText": filterT
    },
    url: "controllers/CrewInquireController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
    var count=0;
    var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
        count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
         responsesHTML += "<td>" + JSONObject[key]["ftype"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["company"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["name"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["email"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["otherinfor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";
        responsesHTML += "<td onClick='deleteCrewInquire("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Infor'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#crewInquiryListDiv").html(responsesHTML);
    }
  });
     //End of ajax
}
$('#searchCrew').keyup(function (e) {
	var selected=$('#searchCrew').val();
	 ajaxSearchCrewInquiries(selected);
})