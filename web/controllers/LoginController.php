<?php
session_start();
class verifyLogin {
  function formValidate() {
  include_once('../config/db_con.php');
//Putting form elements into post variables 
//Data will be sanitised 
  $username=$_POST['username'];
  $password=$_POST['password'];

  $return = array(); 
  $return['msg'] = '';
  $return['error'] = false;
$check=$db_con->prepare("select usr, pass from users where usr=:username && pass=:password");
$check->execute(array(':username'=>$username, ':password'=>$password));
$exist=$check->rowCount();
if($exist==0){ 
   $return['error'] = true;
   $return['msg'] = '<div class="alert alert-danger"><div class="animated flash"><font color="#FF0000">Invalid Login Details</font><i class="glyphicon glyphicon-warning-sign"></i>
</div> 
 </div>';
}
if($return['error'] === false){	
	 $_SESSION['usr']=strtolower($username);
      $return['msg']="logged"; 
	
}
if(empty($password) || !isset($password)){
	   $return['error'] = true;
       $return['msg'] = '<div class="alert alert-warning">Enter Password Please <i class="glyphicon glyphicon-warning-sign"></i>
 </div>';
}
if(empty($username) || !isset($username)){
	   $return['error'] = true;
       $return['msg'] = '<div class="alert alert-warning">Enter Username Please <i class="glyphicon glyphicon-warning-sign"></i>
 </div>';
}
	 //Return json encoded results
	 return json_encode($return);
	}
}
$verifyLogin = new verifyLogin;
echo $verifyLogin->formValidate();
?>