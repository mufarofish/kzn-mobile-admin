// Users javascript

function saveBranch() {
	        var branch=$('#branch').val();
			var adr=$('#adr').val();
	        var rtype="add";	
			 if ((/^\s*$/).test(branch)) {
		     Materialize.toast("Enter branch please", 3000);      
             }
			 else
			 if ((/^\s*$/).test(adr)) {
		     Materialize.toast("Enter location/address", 3000);           
             }			
			else{
			 $.post("controllers/BranchController.php",{
			 rtype: rtype,
			 branch: branch,
			 adr: adr
			 },
			 function(response,status){ // Required Callback Function  		 
			 var stat=response["stat"];
			 var msg=response["msg"];
			 if(stat=="OK"){
			 clearFields();
			 Materialize.toast("Branch created successfully", 3000, '',function(){  ajaxGetSavedBranches(); });  
			 
			 }
			 else{
			 Materialize.toast("Error! " + msg, 6000);      
			 }

			 }		
			
          )};
   
}

function deleteBranch(value, object) {
	var rtype="delete";
	var a = confirm("Are you sure to delete this record?");
     if(a) {
$.post("controllers/BranchController.php",{
       id: value,
	   rtype: rtype
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetSavedBranches();
       }    
)
}
}
function ajaxGetSavedBranches(){
   var rtype="view";
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype
    },
    url: "controllers/BranchController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
	  var optionHTML = "<option value='--Select--'>--Select Branch--</option>";
	  var count=0;
      // Loop through Object and create peopleHTML
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			count++;
	   optionHTML +="<option value='"+JSONObject[key]['branch']+"'>"+JSONObject[key]["branch"]+"</option>";
       responsesHTML += "<tr>";
	   responsesHTML += "<td>" + count + "</td>";
       responsesHTML += "<td>" + JSONObject[key]["branch"] + "</td>";
       responsesHTML += "<td>" + JSONObject[key]["adr"] + "</td>";
	   responsesHTML += "<td onClick='deleteBranch("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Branch'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#branchesListDiv").html(responsesHTML);
	  $("#dpt").html(optionHTML);
	  $("#addempdpt").html(optionHTML);
    }
  });
     //End of ajax
}