<?php
header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
header('Content-type: application/json');
include '../config/DB.php';
include 'CypherController.php';
include 'ObjectValueController.php';
$db = new DB();
$tblName = 'crewinquire';
$type = $_GET['rtype'];
$_POST = json_decode(file_get_contents("php://input"), true);

if(isset($type) && !empty($type)){
    switch($type){
        case "view":
			$orderby='created DESC';
			$filterArray = array('order_by'=>$orderby);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;

		case "viewbyid":
			$orderby='created DESC';
            $where=array('status'=>1);  
            $filterArray = array('where' => $where, 'order_by'=>$orderby);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data = $db->getRows($tblName, $filterArray);
               
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
			
		case "viewbyuser":
			$orderby='created DESC';
			$where=array('posted_by'=>$_GET['id']);	
			$filterArray = array('where' => $where, 'order_by'=>$orderby);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
			
			
        case "add":    
            $posted_by= $_GET['id'];
               $postData = array(
                    'name' => $_POST['name'],
                    'email' => $_POST['email'],
					'company' => $_POST['company'],
                    'sex' => $_POST['sex'],
                    'otherinfor' => $_POST['otherinfor'],
                    'ftype' => $_POST['ftype'],
                    'posted_by' => $posted_by
                );
			  if($postData){
                $insert = $db->insert($tblName,$postData);
                if($insert){
                   
                     $addition ="N/A";
                     if($_POST['otherinfor']){
                         $addition = $_POST['otherinfor'];
                     }
                     $email = "nokuthulas@kwazulunatalfilm.co.za, kznfilmfund@kwazulunatalfilm.co.za ";
                     //$email = "rtakavarasha@gmail.com, mufaroprogrammer@gmail.com";
                     $subject = "Facilities Crew Inquiry";
                     $message = "New Facilities Crew Enquiry has been recieved from  <b>".$_POST['name']."</b>. Client email is : <b>".$_POST['email']."</b> and cell: <b>".$_GET['cell']."</b>. Additional Information: <b>".$$addition."</b>";
                     $from="donotreply@kwazulunatalfilm.co.za";
                     $header = "From:" . $from." \r\n";
                     $header .= "MIME-Version: 1.0\r\n";
                     $header .= "Content-type: text/html\r\n";       
                     $retval = mail ( $email,$subject,$message,$header); 
                     $data['data'] = $insert;
                     $data['stat'] = 'OK';
                     $data['msg'] = 'Request submitted successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
              }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
              }
              echo json_encode($data);
              break;
            default:
            echo '{"status":"INVALID"}';
    }
}