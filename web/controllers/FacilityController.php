<?php
session_start();
header('Content-type: application/json');
include '../config/DB.php';
$db = new DB();
$tblName = 'facilities';
$type=$_POST['rtype'];
if(isset($type) && !empty($type)){
    switch($type){
        case "view":
            $orderby='created DESC';
			$filterArray = array('order_by'=>$orderby);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRowsFacilitiesAll($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
		 case "viewbyid":
            $_SESSION['id'] = $_POST['id'];    
			$where=array('id'=>$_POST['id']);	
			$filterArray = array('where' => $where);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRowsFacilitiesAll($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
         case "viewbystatus":
            $where=array('status'=>$_POST['status']);   
            $filterArray = array('where' => $where);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRowsFacilitiesAll($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
          case "viewbycat":
            $where=array('category'=>$_POST['cat']);   
            $filterArray = array('where' => $where);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRowsFacilitiesAll($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
          case "savesession":
            $_SESSION['id'] = $_POST['id'];    
            $data['stat'] = 'OK';
            echo json_encode($data);
            break;
		 case "search":
			$where=array('category' => $_POST['filterText'], 'infor' => $_POST['filterText'], 'created' => $_POST['filterText']);	
			$filterArray = array('where' => $where);
            $records = $db->getFilterRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRowsFacilitiesId($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
		 case "viewbyname":
			$where=array('company'=>$_POST['cname']);	
			$filterArray = array('where' => $where);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRowsFacilitiesAll($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
         case "savefacilityimage":       
                $updateData = array(
                    'image' => $_SESSION['current_image']
                );
               if($updateData){
                $condition = array('id' => $_SESSION['id']);
                $update = $db->update($tblName,$updateData,$condition);
                if($update){
                    $data['stat'] = 'OK';
                    $data['msg'] = 'Status updated successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
         case "editstatus":       
                $updateData = array(
                    'status' => $_POST['setto']
                );
               if($updateData){
                $condition = array('id' => $_POST['id']);
                $update = $db->update($tblName,$updateData,$condition);
                if($update){
                    $data['stat'] = 'OK';
                    $data['msg'] = 'Status updated successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
         case "editfstatus":       
                $updateData = array(
                    'feature' => $_POST['setto']
                );
               if($updateData){
                $condition = array('id' => $_POST['id']);
                $update = $db->update($tblName,$updateData,$condition);
                if($update){
                    $data['stat'] = 'OK';
                    $data['msg'] = 'Status updated successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
         case "update":       
                $updateData = array(
                     'category' => $_POST['headline'],
                     'infor' => htmlentities($_POST['subtitle']),
                     'contextinfor' => htmlentities($_POST['contextinfor'])
                );
               if($updateData){
                $condition = array('id' => $_SESSION['id']);
                $update = $db->update($tblName,$updateData,$condition);
                if($update){
                    $data['stat'] = 'OK';
                    $data['msg'] = 'Status updated successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
         case "add":         
               $userData = array(
                    'category' => $_POST['headline'],
                    'infor' => htmlentities($_POST['subtitle']),
                    'contextinfor' => htmlentities($_POST['contextinfor']),
                    'image' => $_SESSION['current_image'],
                    'status' => 1           
                );
              if($userData){
                $insert = $db->insert($tblName,$userData);
                if($insert){
                    $data['data'] = $insert;
                    $data['stat'] = 'OK';
                    $data['msg'] = 'User data has been added successfully.';             
                    
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
         case "delete":           
              $condition = array('id' => $_POST['id']);
			  if($condition){
                $delete = $db->delete($tblName,$condition);
                if($delete){
                    $data['stat'] = 'OK';
                    $data['msg'] = 'Data has been deleted successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
        default:
            echo '{"status":"INVALID"}';
    }
}