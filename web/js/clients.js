// Users javascript

function deleteClient(value, object) {
	var rtype="delete";
	var a = confirm("Are you sure to delete this record?");
     if(a) {
$.post("controllers/AppUsersController.php",{
       id: value,
	   rtype: rtype
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetSavedAppUsers();
       }    
)
}
}

function ajaxGetSavedAppUsers(){
 var rtype="view";
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype
    },
    url: "controllers/AppUsersController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
	  var count=0;
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			  count++
        responsesHTML += "<tr>";
	      responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["company"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["name"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["email"] + "</td>";
	      responsesHTML += "<td>" + JSONObject[key]["phone"] + "</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        if(JSONObject[key]["newsletter"]==0){
         responsesHTML += "<td style='color: #F58E25;'>NO</td>";
        }
        if(JSONObject[key]["newsletter"]==1){
         responsesHTML += "<td style='color: #008000;'>YES</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";
	      responsesHTML += "<td onClick='deleteClient("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete User'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#appUsersListDiv").html(responsesHTML);
    }
  });
}

function ajaxGetSavedAppUsersByStatus(status){
   var rtype="viewbystatus";
   if(status==404){
     rtype = "view";
   }
   if(status==0){
     rtype = "viewbystatus";
   }
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype,
      "status": status
    },
    url: "controllers/AppUsersController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
    var count=0;
    var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
        count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["company"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["name"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["email"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["phone"] + "</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        if(JSONObject[key]["newsletter"]==0){
         responsesHTML += "<td style='color: #F58E25;'>NO</td>";
        }
        if(JSONObject[key]["newsletter"]==1){
         responsesHTML += "<td style='color: #008000;'>YES</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";
        responsesHTML += "<td onClick='deleteClient("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete User'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#appUsersListDiv").html(responsesHTML);
    }
  });
}

function ajaxSearchSavedAppUsers(filterText){
   var rtype="search";
   var filterT=filterText;
   $.ajax({
    type: "POST",
    data: {
    "rtype": rtype,
	  "filterText": filterT
    },
    url: "controllers/AppUsersController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
	  var count=0;
      // Loop through Object and create peopleHTML
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			  count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["company"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["name"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["email"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["phone"] + "</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        if(JSONObject[key]["newsletter"]==0){
         responsesHTML += "<td style='color: #F58E25;'>NO</td>";
        }
        if(JSONObject[key]["newsletter"]==1){
         responsesHTML += "<td style='color: #008000;'>YES</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";
        responsesHTML += "<td onClick='deleteClient("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete User'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#appUsersListDiv").html(responsesHTML);
    }
  });
     //End of ajax
}
$('#searchClient').keyup(function (e) {
	var selected=$('#searchClient').val();
	 ajaxSearchSavedAppUsers(selected);
})
 $("#astatusers").change(function () {
	 var selected=$('#astatusers').val();
	 ajaxGetSavedAppUsersByStatus(selected);
 });