// Users javascript

function saveUser() {
	        var name=$('#name').val();
			var email=$('#email').val();
			var username=$('#username').val();
			var password=$('#password').val();
	        var phone=$('#phone').val();
	        var rtype="add";	
			 if ((/^\s*$/).test(name)) {
		     Materialize.toast("Enter name please", 3000);      
             }
			 else
			 if ((/^\s*$/).test(email)) {
		     Materialize.toast("Enter email please", 3000);           
             }			
			else{
			 $.post("controllers/UserController.php",{
			 rtype: rtype,
			 name: name,
			 email:email,
			 username: username,
			 phone:phone,
			 password:password	 
			 },
			 function(response,status){ // Required Callback Function  		 
			 var stat=response["stat"];
			 var msg=response["msg"];
			 if(stat=="OK"){
			 Materialize.toast("Admin account created successfully", 3000, '',function(){  ajaxGetSavedUsers(); });  
			 }
			 else{
			 Materialize.toast("Error! " + msg, 6000);      
			 }

			 }		
			
          )};
   
}

function deleteUser(value, object) {
	var rtype="delete";
	var a = confirm("Are you sure to delete this record?");
     if(a) {
$.post("controllers/UserController.php",{
       id: value,
	   rtype: rtype
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetSavedUsers();
       }    
)
}
}
function ajaxGetSavedUsers(){
   var rtype="view";
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype
    },
    url: "controllers/UserController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
	  var count=0;
      // Loop through Object and create peopleHTML
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			count++
       responsesHTML += "<tr>";
	   responsesHTML += "<td>" + count + "</td>";
       responsesHTML += "<td>" + JSONObject[key]["name"] + "</td>";
       responsesHTML += "<td>" + JSONObject[key]["email"] + "</td>";
       responsesHTML += "<td>" + JSONObject[key]["phone"] + "</td>";
	   responsesHTML += "<td onClick='deleteUser("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete User'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#usersListDiv").html(responsesHTML);
    }
  });
     //End of ajax
}