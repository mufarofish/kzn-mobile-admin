// Users javascript

function payCard() {
	        var jnum=$('#pvjcjnum').text();
			var amt=$('#txtpvjcamt').val();
	        var mpay=$('#mopjc').val();
	        var ref=$('#pvjcref').text();
	        var rtype="add";	
			 if ((/^\s*$/).test(jnum)) {
		     Materialize.toast("Select job please", 3000);      
             }
			 else
			 if ((/^\s*$/).test(amt)) {
		     Materialize.toast("Enter amount please", 3000);           
             }			
			else{
			 $.post("controllers/PaymentController.php",{
			 rtype: rtype,
			 jnum: jnum,
			 mpay:mpay,
			 ref:ref,
			 amt: amt
			 },
			 function(response,status){ // Required Callback Function  		 
			 var stat=response["stat"];
			 var msg=response["msg"];
			 if(stat=="OK"){
			 Materialize.toast("Payment transacted successfully", 3000); 
			 clearFields();
			 printJobCardPayment();
			 }
			 else{
			 Materialize.toast("Error! " + msg, 6000);      
			 }

			 }		
			
          )};
}
function ajaxGetSavedPayments(){
   var rtype="viewbyid";
   var ref=$('#genref').text();
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype,
	  "ref": ref
    },
    url: "controllers/PaymentController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
	  var count=0;
	  var tot=0
      // Loop through Object and create peopleHTML
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			count++
			tot+=Number(JSONObject[key]["amt"]);
       responsesHTML += "<tr>";
       responsesHTML += "<td align=right><font size=2>Payment-" + JSONObject[key]["mpay"] + " on "+ JSONObject[key]["created"] + "</font></td>";
       responsesHTML += "<td align=right><font size=2>R" + JSONObject[key]["amt"] + "</font></td>";
       responsesHTML += "</tr>";
       }         
      }
      $("#paymentsList").html(responsesHTML);
	  $("#invTotalPayments").html("<font size=2><b>R"+tot+"</b></font>");

	  var grandtotalcost=$("#grandInvTotal").text();
	  var totalcost= grandtotalcost.substring(1);
	  var bal= round((totalcost - tot), 2);
	   $("#invBalance").html("<strong>R" +bal+"</strong>");	
    }
  });
     //End of ajax
}
$('#txtpvjcamt').keyup(function (e) {
	var selected=$('#txtpvjcamt').val();
	var selected2=$('#mopjc').val();
	 $('#amtAppend').text("R"+ selected+"("+selected2+")");
})
$('#mopjc').change(function (e) {
	var selected=$('#txtpvjcamt').val();
	var selected2=$('#mopjc').val();
	 $('#amtAppend').text("R"+ selected+"("+selected2+")");
})