<?php
header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
header('Content-type: application/json');
include '../config/DB.php';
include 'CypherController.php';
include 'ObjectValueController.php';
$db = new DB();
$tblName = 'showcase';
$type = $_GET['rtype'];
$_POST = json_decode(file_get_contents("php://input"), true);


if(isset($type) && !empty($type)){
    switch($type){
        case "view":
			$orderby='created DESC';
			$filterArray = array('order_by'=>$orderby);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;

		case "viewbyid":
			$orderby='created DESC';
            $where=array('status'=>1);  
            $filterArray = array('where' => $where, 'order_by'=>$orderby);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data = $db->getRows($tblName, $filterArray);
               
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
			
		case "viewbyuser":
			$orderby='created DESC';
			$where=array('posted_by'=>$_GET['id']);	
			$filterArray = array('where' => $where, 'order_by'=>$orderby);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
        case "add":     
            $trim = str_replace("youtu.be", "www.youtube.com/embed", $_POST['alink']);
            $posted_by= $_GET['id'];
               $postData = array(
                    'film_name' => $_POST['film_name'],
                    'year' => $_POST['year'],
					'director' => $_POST['director'],
				    'main_actor' => $_POST['main_actor'],
                    'alink' => $_POST['alink'],
                    'vlink' => $trim,
                    'posted_by' => $posted_by
                );
			  if($postData){
                $insert = $db->insert($tblName,$postData);
                if($insert){
                    $data['data'] = $insert;
                    $data['stat'] = 'OK';
                    $data['msg'] = 'Request submitted successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
        case "edit":	    
                $updateData = array(
                    'status' => $_POST['setto']
                );
			   if($updateData){
                $condition = array('id' => $_POST['id']);
                $update = $db->update($tblName,$updateData,$condition);
                if($update){
                    $data['stat'] = 'OK';
                    $data['msg'] = 'Showcase edited updated successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
        case "delete":           
              $condition = array('id' => $_POST['id']);
			  if($condition){
                $delete = $db->delete($tblName,$condition);
                if($delete){
                    $data['stat'] = 'OK';
                    $data['msg'] = 'Showcase deleted successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
        default:
            echo '{"status":"INVALID"}';
    }
}