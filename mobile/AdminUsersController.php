<?php
session_start();
header('Content-type: application/json');
include '../config/DB.php';
$db = new DB();
$tblName = 'admin_users';
$type=$_POST['rtype'];
if(isset($type) && !empty($type)){
    switch($type){
        case "view":
            $records = $db->getRows($tblName);
            if($records){
                $data['records'] = $db->getRows($tblName);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
	    case "filter":
			$orderby='created DESC';
			$where=array('name'=>$_POST['filterText'], 'surname'=>$_POST['filterText'], 'prov'=>$_POST['filterText'], 'city'=>$_POST['filterText'], 'speciality'=>$_POST['filterText'], );	
			$filterArray = array('where' => $where, 'order_by'=>$orderby);
            $records = $db->getFilterRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getFilterRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;

        case "viewbyid":
            $orderby='created DESC';
            if(isset($_POST['email'])){
                $where=array('email'=>$_POST['email']);    
            }
            if(isset($_POST['id'])){
                $where=array('id'=>$_POST['id']);   
            }
            $filterArray = array('where' => $where, 'order_by'=>$orderby);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
			
        case "add":         
               $userData = array(
                    'name' => $_POST['name'],
                    'email' => $_POST['email'],
					'phone' => $_POST['phone'],
					'dpt' => $_POST['dpt'],
					'access' => $_POST['access'],
					'usr' => $_POST['username'],
					'pass' => $_POST['password']            
                );
			  if($userData){
                $insert = $db->insert($tblName,$userData);
                if($insert){
                    $data['data'] = $insert;
                    $data['stat'] = 'OK';
                    $data['msg'] = 'User data has been added successfully.';	         
					//Send Email to Dude
					$from="donotreply@gbsc-systems.com";
					 $appLink="https://www.gwerumining.com/GBSC_APP/";
					 $subject= $_POST['name']. " Welcome to GBSC";
					 $message = "Hi  ".$_POST['name'] .  ". You have been registered by your admin to the GBSC online system. Please visit this link to use the system and change your password:  " .$appLink. ". Use your login username and password to use the system. Your username is :  [".$_POST['username']."]   and your password is : [".$_POST['password']."]. Enjoy the your day !!!! [DO NOT REPLY THIS EMAIL]"."\r\n";
                     $header = "From:" . $from." \r\n";
                     $header .= "MIME-Version: 1.0\r\n";
                     $header .= "Content-type: text/html\r\n";       
                     $retval = mail ( $_POST['email'],$subject,$message,$header);
					
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
        case "edit":
            
                $userData = array(
                    'name' => $_POST['data']['name'],
                    'email' => $_POST['data']['email'],
                    'phone' => $_POST['data']['phone']
                );
			   if($userData){
                $condition = array('id' => $_POST['data']['id']);
                $update = $db->update($tblName,$userData,$condition);
                if($update){
                    $data['stat'] = 'OK';
                    $data['msg'] = 'User data has been updated successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
        case "delete":           
              $condition = array('id' => $_POST['id']);
			  if($condition){
                $delete = $db->delete($tblName,$condition);
                if($delete){
                    $data['stat'] = 'OK';
                    $data['msg'] = 'User data has been deleted successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
        default:
            echo '{"status":"INVALID"}';
    }
}