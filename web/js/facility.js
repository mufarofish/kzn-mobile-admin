// Users javascript

function deleteFacility(value, object) {
	var rtype="delete";
	var a = confirm("Are you sure to delete this record?");
     if(a) {
$.post("controllers/FacilityController.php",{
       id: value,
	   rtype: rtype
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetFacility();
       }    
)
}
}

function editFacility(id){
  var rtype="viewbyid";
   $.ajax({
    type: "POST",
    data: {
    "rtype": rtype,
    "id": id
    },
    url: "controllers/FacilityController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
    var count=0;
      // Loop through Object and create peopleHTML
    var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
       if (JSONObject.hasOwnProperty(key)) {
      count++ 
      //Put car details
        $('#txteditfacilityheadlines').val(JSONObject[key]["category"]);
        $('#txteditfacilitysubtitle').val(JSONObject[key]["infor"]);
        $('textarea#txteditfacilitycontextinfor').val(JSONObject[key]["contextinfor"]);
       }         
      }
      loadViewEditFacility();    
    }
  });
}

function changeFacilityStatus(idToUpdate, setStatusTo, refreshStatus) {
var rtype="editstatus";
$.post("controllers/FacilityController.php",{
       id: idToUpdate,
       rtype: rtype,
       setto: setStatusTo
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetFacilityByStatus(refreshStatus); 
      }    
)
}

function changeFacilityFeatureStatus(idToUpdate, setStatusTo, refreshStatus) {
var rtype="editfstatus";
$.post("controllers/FacilityController.php",{
       id: idToUpdate,
       rtype: rtype,
       setto: setStatusTo
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetFacility();
      }    
)
}

function editPicFacility(id){
    var rtype = 'savesession';
    $.post("controllers/FacilityController.php",{
       id: id,
       rtype: rtype
       },
       function(response,status){ // Required Callback Function     
        $('#editFacilityPicPanel').hide(); 
        $('#editFacilityPicPanel').show("slow");
      }    
)
  $('html,body,window').animate({scrollTop: $("#editFacilityPicPanel").offset().top},'slow');
}

function saveEditFacilityImage(){
   var rtype = 'savefacilityimage';
    $.post("controllers/FacilityController.php",{
       rtype: rtype
       },
       function(response,status){ // Required Callback Function     
        $('#editFacilityPicPanel').hide("slow"); 
      }    
)
}


function ajaxGetFacility(){
 var rtype="view";
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype
    },
    url: "controllers/FacilityController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
	  var count=0;
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			  count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["category"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["infor"] + "</td>";
        var cont = JSONObject[key]["contextinfor"];
        var text = cont.substr(0, 20) + "...";
        responsesHTML += "<td>"+ text +"</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";   
        if(JSONObject[key]["feature"]==0){
         responsesHTML += "<td onClick='changeFacilityFeatureStatus("+JSONObject[key]["id"]+", 1, 0)'><span class='badge badge-secondary' style='cursor:pointer;'>Not Featured</span></td>";
        }
        if(JSONObject[key]["feature"]==1){
         responsesHTML += "<td onClick='changeFacilityFeatureStatus("+JSONObject[key]["id"]+", 0, 1)'><span class='badge badge-success' style='background-color:#28A745;cursor:pointer;'>Featured</span></td>";
        }
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeFacilityStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeFacilityStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        var link = getImageLink();
        responsesHTML += "<td><a href= "+link+JSONObject[key]["image"] +" target='_blank'>View Image</a></td>";
        responsesHTML += "<td onClick='editPicFacility("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Image</td>";
        responsesHTML += "<td onClick='editFacility("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Text</td>";
        responsesHTML += "<td onClick='deleteFacility("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#facilityListDiv").html(responsesHTML);
    }
  });
}

function ajaxGetFacilityByStatus(status){
   var rtype="viewbystatus";
   if(status==404){
     rtype = "view";
   }
   if(status==0){
     rtype = "viewbystatus";
   }
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype,
      "status": status
    },
    url: "controllers/FacilityController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
    var count=0;
    var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
        count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["category"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["infor"] + "</td>";
        var cont = JSONObject[key]["contextinfor"];
        var text = cont.substr(0, 20) + "...";
        responsesHTML += "<td>"+ text +"</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";   
        if(JSONObject[key]["feature"]==0){
         responsesHTML += "<td onClick='changeFacilityFeatureStatus("+JSONObject[key]["id"]+", 1, 0)'><span class='badge badge-secondary' style='cursor:pointer;'>Not Featured</span></td>";
        }
        if(JSONObject[key]["feature"]==1){
         responsesHTML += "<td onClick='changeFacilityFeatureStatus("+JSONObject[key]["id"]+", 0, 1)'><span class='badge badge-success' style='background-color:#28A745;cursor:pointer;'>Featured</span></td>";
        }
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeFacilityStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeFacilityStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        var link = getImageLink();
        responsesHTML += "<td><a href= "+link+JSONObject[key]["image"] +" target='_blank'>View Image</a></td>";
        responsesHTML += "<td onClick='editPicFacility("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Image</td>";
        responsesHTML += "<td onClick='editFacility("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Text</td>";
        responsesHTML += "<td onClick='deleteFacility("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#facilityListDiv").html(responsesHTML);
    }
  });
}

function ajaxSearchFacility(filterText){
   var rtype="search";
   var filterT=filterText;
   $.ajax({
    type: "POST",
    data: {
    "rtype": rtype,
	  "filterText": filterT
    },
    url: "controllers/FacilityController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
	  var count=0;
      // Loop through Object and create peopleHTML
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
        count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["category"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["infor"] + "</td>";
        var cont = JSONObject[key]["contextinfor"];
        var text = cont.substr(0, 20) + "...";
        responsesHTML += "<td>"+ text +"</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";   
        if(JSONObject[key]["feature"]==0){
         responsesHTML += "<td onClick='changeFacilityFeatureStatus("+JSONObject[key]["id"]+", 1, 0)'><span class='badge badge-secondary' style='cursor:pointer;'>Not Featured</span></td>";
        }
        if(JSONObject[key]["feature"]==1){
         responsesHTML += "<td onClick='changeFacilityFeatureStatus("+JSONObject[key]["id"]+", 0, 1)'><span class='badge badge-success' style='background-color:#28A745;cursor:pointer;'>Featured</span></td>";
        }
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeFacilityStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeFacilityStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        var link = getImageLink();
        responsesHTML += "<td><a href= "+link+JSONObject[key]["image"] +" target='_blank'>View Image</a></td>";
        responsesHTML += "<td onClick='editPicFacility("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Image</td>";
        responsesHTML += "<td onClick='editFacility("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Text</td>";
        responsesHTML += "<td onClick='deleteFacility("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#facilityListDiv").html(responsesHTML);
    }
  });
     //End of ajax
}
$('#searchFacility').keyup(function (e) {
	var selected=$('#searchFacility').val();
	 ajaxSearchFacility(selected);
})
 $("#astatfacility").change(function () {
	   var selected=$('#astatfacility').val();
     ajaxGetFacilityByStatus(selected);
 });
//Saving and Updating
function createFacilityFacility() {
      var headlines=$('#txtfacilityheadlines').val();
      var subtitle=$('#txtfacilitysubtitle').val();
      var contextinfor=$('#txtfacilitycontextinfor').val();
      var rtype="add";  
      if ((/^\s*$/).test(headlines)) {
         Materialize.toast("Enter headlines", 3000);      
             }
      else
      if ((/^\s*$/).test(contextinfor)) {
         Materialize.toast("Enter context information.", 3000);           
      } else {
       $.post("controllers/FacilityController.php",{
       rtype: rtype,
       headline: headlines,
       subtitle:subtitle,
       contextinfor:contextinfor
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       if(stat=="OK"){
       Materialize.toast("Information posted successfully", 3000, '',function(){  clearFields(); });  
       }
       else{
       Materialize.toast("Error! " + msg, 6000);      
       }

       }    
       )};
}

function updateFacilityFacility() {
      var headlines=$('#txteditfacilityheadlines').val();
      var subtitle=$('#txteditfacilitysubtitle').val();
      var contextinfor=$('#txteditfacilitycontextinfor').val();
      var rtype="update";  
      if ((/^\s*$/).test(headlines)) {
         Materialize.toast("Enter headlines", 3000);      
             }
      else
      if ((/^\s*$/).test(contextinfor)) {
         Materialize.toast("Enter context information.", 3000);           
      } else {
       $.post("controllers/FacilityController.php",{
       rtype: rtype,
       headline: headlines,
       subtitle:subtitle,
       contextinfor:contextinfor
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       if(stat=="OK"){
       Materialize.toast("Information updated successfully", 3000, '',function(){  clearFields(); });  
       }
       else{
       Materialize.toast("Error! " + msg, 6000);      
       }
       }    
     )};
}