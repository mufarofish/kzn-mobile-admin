// Users javascript

function deleteBursaryInquire(value, object) {
	var rtype="delete";
	var a = confirm("Are you sure to delete this record?");
     if(a) {
$.post("controllers/BursaryInquireController.php",{
       id: value,
	   rtype: rtype
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetBursaryInquiries();
       }    
)
}
}

function ajaxGetBursaryInquiries(){
 var rtype="view";
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype
    },
    url: "controllers/BursaryInquireController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
	  var count=0;
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			  count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["name"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["sex"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["email"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["age"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["sfield"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["syear"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["boption"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["otherinfor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";
        responsesHTML += "<td onClick='deleteBursaryInquire("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete User'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#busInquiryListDiv").html(responsesHTML);
    }
  });
}

function ajaxSearchBursaryInquiries(filterText){
   var rtype="search";
   var filterT=filterText;
   $.ajax({
    type: "POST",
    data: {
    "rtype": rtype,
	  "filterText": filterT
    },
    url: "controllers/BursaryInquireController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
	  var count=0;
      // Loop through Object and create peopleHTML
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			  count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["name"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["sex"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["email"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["age"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["sfield"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["syear"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["boption"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["otherinfor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";
        responsesHTML += "<td onClick='deleteBursaryInquire("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete User'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#busInquiryListDiv").html(responsesHTML);
    }
  });
     //End of ajax
}
$('#searchBursary').keyup(function (e) {
	var selected=$('#searchBursary').val();
	 ajaxSearchBursaryInquiries(selected);
})
 $("#cjcsclient").change(function () {
	 var selected=$('#cjcsclient').val();
	 $('.carinfor').text("Select Car");
     ajaxJobGetSavedClientsById(selected);
	 ajaxGetOwnerCars(selected);
 });