<?php
session_start();
header('Content-type: application/json');
include '../config/DB.php';
$db = new DB();
$tblName = 'showcase';
$type=$_POST['rtype'];
if(isset($type) && !empty($type)){
    switch($type){
        case "view":
            $orderby='created DESC';
			$filterArray = array('order_by'=>$orderby);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
		 case "viewbyid":
            $_SESSION['id'] = $_POST['id'];
			$where=array('id'=>$_POST['id']);	
			$filterArray = array('where' => $where);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
         case "viewbystatus":
            $where=array('status'=>$_POST['status']);   
            $filterArray = array('where' => $where);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
		 case "search":
			$where=array('film_name' => $_POST['filterText'], 'year' => $_POST['filterText'], 'director' => $_POST['filterText'], 'main_actor' => $_POST['filterText'], 'created' => $_POST['filterText']);	
			$filterArray = array('where' => $where);
            $records = $db->getFilterRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getFilterRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
		 case "viewbyname":
			$where=array('company'=>$_POST['cname']);	
			$filterArray = array('where' => $where);
            $records = $db->getRows($tblName, $filterArray);
            if($records){
                $data['records'] = $db->getRows($tblName, $filterArray);
                $data['stat'] = 'OK';
            }else{
                $data['records'] = array();
                $data['stat'] = 'ERR';
            }
            echo json_encode($data);
            break;
         case "editstatus":       
                $updateData = array(
                    'status' => $_POST['setto']
                );
               if($updateData){
                $condition = array('id' => $_POST['id']);
                $update = $db->update($tblName,$updateData,$condition);
                if($update){
                    $data['stat'] = 'OK';
                    $data['msg'] = 'Status updated successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
        case "update":  
                $trim = str_replace("youtu.be", "www.youtube.com/embed", $_POST['link']);     
                $updateData = array(
                     'film_name' => $_POST['film_name'],
                     'director' => $_POST['director'],
                     'main_actor' => $_POST['main_actor'],
                     'year' => $_POST['year'],
                     'alink' => $_POST['link'],
                     'vlink' => $trim
                );
               if($updateData){
                $condition = array('id' => $_SESSION['id']);
                $update = $db->update($tblName,$updateData,$condition);
                if($update){
                    $data['stat'] = 'OK';
                    $data['msg'] = 'Status updated successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
        case "delete":           
              $condition = array('id' => $_POST['id']);
			  if($condition){
                $delete = $db->delete($tblName,$condition);
                if($delete){
                    $data['stat'] = 'OK';
                    $data['msg'] = 'Client data has been deleted successfully.';
                }else{
                    $data['stat'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $data['stat'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            break;
        default:
            echo '{"status":"INVALID"}';
    }
}