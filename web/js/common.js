
$(function () {
	$("#dropBox").click(function(){
        $("#fileInput").click();
    });
    $("#edropBox").click(function(){
        $("#efileInput").click();
    });
    $("#csdropBox").click(function(){
        $("#csfileInput").click();
    });
    $("#esdropBox").click(function(){
        $("#esfileInput").click();
    });
    $("#efdropBox").click(function(){
        $("#effileInput").click();
    });
    $("#cfdropBox").click(function(){
        $("#cffileInput").click();
    });
    $("#cldropBox").click(function(){
        $("#clfileInput").click();
    });
     $("#eldropBox").click(function(){
        $("#elfileInput").click();
    });
    
    //prevent browsers from opening the file when its dragged and dropped
    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });

    //call a function to handle file upload on select file
    $('input[type=file]#fileInput').on('change', fileUpload);
    $('input[type=file]#efileInput').on('change', efileUpload);
    $('input[type=file]#csfileInput').on('change', csfileUpload);
    $('input[type=file]#esfileInput').on('change', esfileUpload);
    $('input[type=file]#cffileInput').on('change', cffileUpload);
    $('input[type=file]#effileInput').on('change', effileUpload);
    $('input[type=file]#clfileInput').on('change', clfileUpload);
    $('input[type=file]#elfileInput').on('change', elfileUpload);
});
function getImageLink(){
   var link= 'https://www.kwazulunatalfilm.co.za/kznapp/web/AppImages/';
   return link;
}
function loadUsers(){
    hideViews();
	$('#userPanel').show("slow");
	ajaxGetSavedUsers();
}
function loadAppUsers(){
    hideViews();
	$('#appUsersPanel').show("slow");
	ajaxGetSavedAppUsers();
}
function loadViewLocationInquiry(){
    hideViews();
	$('#locationInquiryPanel').show("slow");
	ajaxGetLocationInquiries();
}
function loadViewBursaryInquiry(){
    hideViews();
	$('#bursaryInquiryPanel').show("slow");
	ajaxGetBursaryInquiries();
}
function loadViewFacilityInquiry(){
    hideViews();
	$('#facilityInquiryPanel').show("slow");
	ajaxGetFacilityInquiries();
}
function loadViewFundingInquiry(){
    hideViews();
	$('#fundingInquiryPanel').show("slow");
	ajaxGetFundingInquiries();
}

function loadViewTrainingInquiry(){
    hideViews();
    $('#trainingInquiryPanel').show("slow");
    ajaxGetTrainingInquiries();
}

function loadViewCrewInquiry(){
    hideViews();
    $('#crewInquiryPanel').show("slow");
    ajaxGetCrewInquiries();
}

function loadViewShowcase(){
    hideViews();
	$('#showcaseListPanel').show("slow");
	ajaxGetShowcase();
}

function loadViewEvents(){
    hideViews();
	$('#eventsListPanel').show("slow");
	ajaxGetEvents();
}
function loadViewEditEvents(){
    hideViews();
	$('#editNewsPanel').show("slow");
}
function loadCreateNewsEvents(){
    hideViews();
	$('#createNewsPanel').show("slow");
}
function loadViewLocation(){
    hideViews();
    $('#locationListPanel').show("slow");
    ajaxGetLocation();
}
function loadViewEditLocation(){
    hideViews();
    $('#editLocationPanel').show("slow");
}
function loadCreateLocationLocation(){
    hideViews();
    $('#createLocationPanel').show("slow");
}

function loadViewSocial(){
    hideViews();
	$('#socialListPanel').show("slow");
	ajaxGetSocial();
}
function loadViewEditSocial(){
    hideViews();
	$('#editSocialPanel').show("slow");
}
function loadCreateSocialSocial(){
    hideViews();
	$('#createSocialPanel').show("slow");
}

function loadViewFacility(){
    hideViews();
	$('#facilityListPanel').show("slow");
	ajaxGetFacility();
}
function loadViewEditFacility(){
    hideViews();
	$('#editFacilityPanel').show("slow");
}
function loadCreateFacilityFacility(){
    hideViews();
	$('#createFacilityPanel').show("slow");
}
function loadViewEditShowcase(){
    hideViews();
    $('#editShowcasePanel').show("slow");
}

function loadDashboard(){
	hideViews();
	$('.dashboardPanel').show("slow");
}

function manualMessage(){
     Materialize.toast('System manual coming soon..', 3000);
}

function fileUpload(event){
    //notify user about the file upload status
    $("#dropBox").html(event.target.value+" uploading...");
    
    //get selected file
    files = event.target.files;
    
    //form data check the above bullet for what it is  
    var data = new FormData();                                   

    //file data is presented as an array
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        /*if(!file.type.match('application/docx')) {              
            //check file type
            $("#dropBox").html("Please choose a document file.");
        }else 
        */
        if(file.size > 1048576){
            //check file size (in bytes)
            $("#dropBox").html("Sorry, your file is too large (>1 MB)");
        }else{
            //append the uploadable file to FormData object
            data.append('file', file, file.name);
            
            //create a new XMLHttpRequest
            var xhr = new XMLHttpRequest();     
            
            //post file data for upload
            xhr.open('POST', 'upload.php', true);  
            xhr.send(data);
            xhr.onload = function () {
                //get response and show the uploading status
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#dropBox").html("File ready!. Click save.");
                }else if(response.status == 'type_err'){
                    $("#dropBox").html("Please select an image file");
                }else{
                    $("#dropBox").html("Some problem occured, please try again.");
                }
            };
        }
    }
}

function efileUpload(event){
    //notify user about the file upload status
    $("#edropBox").html(event.target.value+" uploading...");
    
    //get selected file
    files = event.target.files;
    
    //form data check the above bullet for what it is  
    var data = new FormData();                                   

    //file data is presented as an array
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        /*if(!file.type.match('application/docx')) {              
            //check file type
            $("#dropBox").html("Please choose a document file.");
        }else 
        */
        if(file.size > 1048576){
            //check file size (in bytes)
            $("#edropBox").html("Sorry, your file is too large (>1 MB)");
        }else{
            //append the uploadable file to FormData object
            data.append('file', file, file.name);
            
            //create a new XMLHttpRequest
            var xhr = new XMLHttpRequest();     
            
            //post file data for upload
            xhr.open('POST', 'upload.php', true);  
            xhr.send(data);
            xhr.onload = function () {
                //get response and show the uploading status
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#edropBox").html("File ready!. Click save.");
                }else if(response.status == 'type_err'){
                    $("#edropBox").html("Please select an image file");
                }else{
                    $("#edropBox").html("Some problem occured, please try again.");
                }
            };
        }
    }
}


function csfileUpload(event){
    //notify user about the file upload status
    $("#csdropBox").html(event.target.value+" uploading...");
    
    //get selected file
    files = event.target.files;
    
    //form data check the above bullet for what it is  
    var data = new FormData();                                   

    //file data is presented as an array
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        /*if(!file.type.match('application/docx')) {              
            //check file type
            $("#dropBox").html("Please choose a document file.");
        }else 
        */
        if(file.size > 1048576){
            //check file size (in bytes)
            $("#csdropBox").html("Sorry, your file is too large (>1 MB)");
        }else{
            //append the uploadable file to FormData object
            data.append('file', file, file.name);
            
            //create a new XMLHttpRequest
            var xhr = new XMLHttpRequest();     
            
            //post file data for upload
            xhr.open('POST', 'upload.php', true);  
            xhr.send(data);
            xhr.onload = function () {
                //get response and show the uploading status
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#csdropBox").html("File ready!. Click save.");
                }else if(response.status == 'type_err'){
                    $("#csdropBox").html("Please select an image file");
                }else{
                    $("#csdropBox").html("Some problem occured, please try again.");
                }
            };
        }
    }
}

function esfileUpload(event){
    //notify user about the file upload status
    $("#efdropBox").html(event.target.value+" uploading...");
    
    //get selected file
    files = event.target.files;
    
    //form data check the above bullet for what it is  
    var data = new FormData();                                   

    //file data is presented as an array
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        /*if(!file.type.match('application/docx')) {              
            //check file type
            $("#dropBox").html("Please choose a document file.");
        }else 
        */
        if(file.size > 1048576){
            //check file size (in bytes)
            $("#esdropBox").html("Sorry, your file is too large (>1 MB)");
        }else{
            //append the uploadable file to FormData object
            data.append('file', file, file.name);
            
            //create a new XMLHttpRequest
            var xhr = new XMLHttpRequest();     
            
            //post file data for upload
            xhr.open('POST', 'upload.php', true);  
            xhr.send(data);
            xhr.onload = function () {
                //get response and show the uploading status
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#esdropBox").html("File ready!. Click save.");
                }else if(response.status == 'type_err'){
                    $("#esdropBox").html("Please select an image file");
                }else{
                    $("#esdropBox").html("Some problem occured, please try again.");
                }
            };
        }
    }
}

function cffileUpload(event){
    //notify user about the file upload status
    $("#cfdropBox").html(event.target.value+" uploading...");
    
    //get selected file
    files = event.target.files;
    
    //form data check the above bullet for what it is  
    var data = new FormData();                                   

    //file data is presented as an array
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        /*if(!file.type.match('application/docx')) {              
            //check file type
            $("#dropBox").html("Please choose a document file.");
        }else 
        */
        if(file.size > 1048576){
            //check file size (in bytes)
            $("#cfdropBox").html("Sorry, your file is too large (>1 MB)");
        }else{
            //append the uploadable file to FormData object
            data.append('file', file, file.name);
            
            //create a new XMLHttpRequest
            var xhr = new XMLHttpRequest();     
            
            //post file data for upload
            xhr.open('POST', 'upload.php', true);  
            xhr.send(data);
            xhr.onload = function () {
                //get response and show the uploading status
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#cfdropBox").html("File ready!. Click save.");
                }else if(response.status == 'type_err'){
                    $("#cfdropBox").html("Please select an image file");
                }else{
                    $("#cfdropBox").html("Some problem occured, please try again.");
                }
            };
        }
    }
}

function effileUpload(event){
    //notify user about the file upload status
    $("#efdropBox").html(event.target.value+" uploading...");
    
    //get selected file
    files = event.target.files;
    
    //form data check the above bullet for what it is  
    var data = new FormData();                                   

    //file data is presented as an array
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        /*if(!file.type.match('application/docx')) {              
            //check file type
            $("#dropBox").html("Please choose a document file.");
        }else 
        */
        if(file.size > 1048576){
            //check file size (in bytes)
            $("#efdropBox").html("Sorry, your file is too large (>1 MB)");
        }else{
            //append the uploadable file to FormData object
            data.append('file', file, file.name);
            
            //create a new XMLHttpRequest
            var xhr = new XMLHttpRequest();     
            
            //post file data for upload
            xhr.open('POST', 'upload.php', true);  
            xhr.send(data);
            xhr.onload = function () {
                //get response and show the uploading status
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#efdropBox").html("File ready!. Click save.");
                }else if(response.status == 'type_err'){
                    $("#efdropBox").html("Please select an image file");
                }else{
                    $("#efdropBox").html("Some problem occured, please try again.");
                }
            };
        }
    }
}
function elfileUpload(event){
    //notify user about the file upload status
    $("#eldropBox").html(event.target.value+" uploading...");
    
    //get selected file
    files = event.target.files;
    
    //form data check the above bullet for what it is  
    var data = new FormData();                                   

    //file data is presented as an array
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        /*if(!file.type.match('application/docx')) {              
            //check file type
            $("#dropBox").html("Please choose a document file.");
        }else 
        */
        if(file.size > 1048576){
            //check file size (in bytes)
            $("#eldropBox").html("Sorry, your file is too large (>1 MB)");
        }else{
            //append the uploadable file to FormData object
            data.append('file', file, file.name);
            
            //create a new XMLHttpRequest
            var xhr = new XMLHttpRequest();     
            
            //post file data for upload
            xhr.open('POST', 'upload.php', true);  
            xhr.send(data);
            xhr.onload = function () {
                //get response and show the uploading status
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#eldropBox").html("File ready!. Click save.");
                }else if(response.status == 'type_err'){
                    $("#eldropBox").html("Please select an image file");
                }else{
                    $("#eldropBox").html("Some problem occured, please try again.");
                }
            };
        }
    }
}

function clfileUpload(event){
    //notify user about the file upload status
    $("#cldropBox").html(event.target.value+" uploading...");
    
    //get selected file
    files = event.target.files;
    
    //form data check the above bullet for what it is  
    var data = new FormData();                                   

    //file data is presented as an array
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        /*if(!file.type.match('application/docx')) {              
            //check file type
            $("#dropBox").html("Please choose a document file.");
        }else 
        */
        if(file.size > 1048576){
            //check file size (in bytes)
            $("#cldropBox").html("Sorry, your file is too large (>1 MB)");
        }else{
            //append the uploadable file to FormData object
            data.append('file', file, file.name);
            
            //create a new XMLHttpRequest
            var xhr = new XMLHttpRequest();     
            
            //post file data for upload
            xhr.open('POST', 'upload.php', true);  
            xhr.send(data);
            xhr.onload = function () {
                //get response and show the uploading status
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#cldropBox").html("File ready!. Click save.");
                }else if(response.status == 'type_err'){
                    $("#cldropBox").html("Please select an image file");
                }else{
                    $("#cldropBox").html("Some problem occured, please try again.");
                }
            };
        }
    }
}
function loadPayJobCard(object, jobnumber, ref){
	//Load necesary views
	$('#pvjcjnum').text(jobnumber);
	$('#pvjcref').text(ref);
    showJobCardPay();
    $('html, body').animate({
    scrollTop: $('html, body').offset().top - document.body.clientHeight + $('html, body').height()
}, 1000);
}
//Payslip
function payslipInitialCreate(){
	$('#actualPayslip').show("slow");
}
function openAddPayItems(){
	$('#payslipAddressDiv').hide();
	$('#addPayItemsDiv').show("slow");
}
function closeAddPayItems(){
	$('#addPayItemsDiv').hide();
	$('#payslipAddressDiv').show("slow");
}
function addQuoteItems(object, id, clientid, carid, ref){
    var jobnumber=$('#xxjnum').text();
	$('#genref').html("<font size=2>"+ref+"</font>");
	$('#invnumber').html("<font size=2>"+id+"</font>");
	$('#invintitle').html("<font size=2><strong>QuoteNo.</strong></font>");
	$('#xxcid').text(clientid);
	$('#invmaintitle').text("QUOTE");
	hideViews();
	$('.forInvoice').hide();
	$('.forQuote').show();
	ajaxGetSavedClientsById(clientid);
	ajaxGetInvoiceCarById(carid);
	ajaxGetSavedItemsById(ref);
	$('#createInvoicePanel').show("slow");
	$('#addInvoiceItemsDiv').hide();	
}
function loadAddBranches(){
	hideViews();
	$('#createBranchesPanel').show("slow");
	ajaxGetSavedBranches();

}
function rptInvRefresh(){
	$('.rptTitle').hide();
	$('.rptAllowed').hide();
	$('.rptNotAllowed').show();
	$('#invrpttitle').val('');
}
function openAddCars(id, object){
	hideViews();
	$('#carsPanel').show("slow");
	$('#sv_clientid').text(id.toString());
	ajaxGetOwnerCars(id);	
}
function loadAddClients(){
	hideViews();
	$('#createClientsPanel').show("slow");	
	ajaxGetSavedClients();
	//$('#clientsTable').dataTable();
}
function loadAddSalaryGrade(){
	hideViews();
	$('#createPayslipsPanel').show("slow");	
	ajaxGetSavedSalaryGrades();
}
function loadAddInvoice(object, id, clientid, carid, ref){
	//Genarate ref
	//var jobnumber=$('#xxjnum').text();
	$('#genref').html("<font size=2>"+ref+"</font>");
	$('#invnumber').html("<font size=2>"+id+"</font>");
	$('#invintitle').html("<font size=2><strong>InvoiceNo.</strong></font>");
	$('#xxcid').text(clientid);
	$('#invmaintitle').text("INVOICE");
	hideViews();
	$('.forInvoice').show();
	$('.forQuote').hide();
	ajaxGetSavedClientsById(clientid);
	ajaxGetInvoiceCarById(carid);
	ajaxGetSavedItemsById(ref);
	$('#createInvoicePanel').show("slow");
	$('#addInvoiceItemsDiv').hide();	
	 $('html, body').animate({
    scrollTop: $('html, body').offset().top - document.body.clientHeight + $('html, body').height()
}, 1000);
}
function loadAddQuoteItems(object, id, clientid, carid, ref){
	//hideViews();
	/*$('.jc').show();
	$('.qt').hide();
	$('#jcqtitle').text("Create Job Card");
	ajaxGetCarById(carid);
	ajaxJobGetSavedClientsById(clientid);
	*/
	//$('#bookPanel').show("slow");
}
function loadCreateJobCard(object, id, clientid, carid, ref){
	hideViews();
	$('.jc').show();
	$('.qt').hide();
	$('#jcqtitle').text("Create Job Card");
	$('#fref').text(ref);
	$('#fcarid').text(""+carid);
	$('#fcid').text(""+clientid);
	ajaxGetCarById(carid);
	ajaxJobGetSavedClientsById(clientid);
	$('#bookPanel').show("slow");
}
function loadCreateQuote(){
	hideViews();
	ajaxJobGetSavedClients();
	$('.qt').show();
	$('.jc').hide();
	$('#jcqtitle').text("Create Quote");
	$('#bookPanel').show("slow");	
}
function loadAddEmployee(){
	hideViews();
	ajaxGetSavedEmployees();
	ajaxGetSavedBranches();
	ajaxGetSavedSalaryGrades();
	$('#addEmployeePanel').show("slow");
}
function loadCreateUpdateCard(){
	hideViews();
	ajaxJobGetSavedClients();
	$('#cardPanel').show("slow");
}
function loadBookViews(){
	var num="0";
	hideViews();
	$('#viewBookings').show("slow");
	$("#rstat").val(0).change();
	ajaxGetBookings(num);
}
function loadQuotesViews(){
	var num="0";
	hideViews();
	$('#viewQuotesPanel').show("slow");
	$("#qrstat").val(0).change();
	ajaxGetQuotes(num);
}

function showBookingDetails(){
	$('#viewBookingDetails').slideDown("slow");
}
function hideBookingDetails(){
	$('#viewBookingDetails').hide();
}
function loadQuoteReports(){
	 	
}
function loadCapturePayments(){
	     hideViews();
	     ajaxJobGetSavedClients();
	     $('#cardPaymentsPanel').show();
}
function loadCardMembers(){
	 	 hideViews();
	 	 ajaxGetCardByStatus(1);
	 	 $('#viewCards').show();
}
function loadInvoiceReports(){
	     hideViews();
	     ajaxGetInvoiceReport();
	     $('#invoiceRptPanel').show();	
}
function loadEmployeesPayslips(){
	     hideViews();
	     ajaxGetEmpListPayslips();
	     $('#initialListPayslipPanel').show();	
}
function openEmployeePayslips(name, ecn, phone, email, idn, grade){
	hideViews();
	$('#createPayslipPanel').show();
	$('#payname').html('<font size=2>'+name+'</font>');
	$('#payecn').html('<font size=2>'+ecn+'</font>');
	$('#payphone').html('<font size=2>'+phone+'</font>');
	$('#payemail').html('<font size=2>'+email+'</font>');
	$('#payidn').html('<font size=2>'+idn+'</font>');
	$('#paygrade').html('<font size=2>'+grade+'</font>');
	findGradeSalary(grade);
}
function hideViews(){
	$('.dashboardPanel').hide();
	$('#userPanel').hide();
	$('#locationInquiryPanel').hide();
	$('#appUsersPanel').hide();
	$('#facilityInquiryPanel').hide();
	$('#bursaryInquiryPanel').hide();
	$('#fundingInquiryPanel').hide();
	$('#showcaseListPanel').hide();
	$('#eventsListPanel').hide();
	$('#editEventPicPanel').hide();
	$('#createNewsPanel').hide();
	$('#editNewsPanel').hide();
	$('#socialListPanel').hide();
	$('#editSocialPicPanel').hide();
	$('#createSocialPanel').hide();
	$('#editSocialPanel').hide();
	$('#facilityListPanel').hide();
	$('#editFacilityPicPanel').hide();
	$('#createFacilityPanel').hide();
	$('#editFacilityPanel').hide();
    $('#locationListPanel').hide();
    $('#editLocationPicPanel').hide();
    $('#createLocationPanel').hide();
    $('#editLocationPanel').hide();
    $('#editShowcasePanel').hide();
    $('#trainingInquiryPanel').hide();
    $('#crewInquiryPanel').hide();
}
function clearFields(){
	$('.apm-text-field').val('');
}
function hideInvoice(){
	$('#actualInvoice').hide();
}
function showInvoice(){
	$('#actualInvoice').show("slow");
}
function generateInvoice(){
	$('#actualInvoice').show("slow");
}