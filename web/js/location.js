// Users javascript

function deleteLocation(value, object) {
	var rtype="delete";
	var a = confirm("Are you sure to delete this record?");
     if(a) {
$.post("controllers/LocationController.php",{
       id: value,
	   rtype: rtype
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetLocation();
       }    
)
}
}

function editLocation(id){
  var rtype="viewbyid";
   $.ajax({
    type: "POST",
    data: {
    "rtype": rtype,
    "id": id
    },
    url: "controllers/LocationController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
    var count=0;
      // Loop through Object and create peopleHTML
    var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
       if (JSONObject.hasOwnProperty(key)) {
      count++ 
      //Put car details
        $('#txteditlocationheadlines').val(JSONObject[key]["place"]);
        $('#txteditlocationsubtitle').val(JSONObject[key]["infor"]);
        $('textarea#txteditlocationcontextinfor').val(JSONObject[key]["contextinfor"]);
       }         
      }
      loadViewEditLocation();    
    }
  });
}

function changeLocationStatus(idToUpdate, setStatusTo, refreshStatus) {
var rtype="editstatus";
$.post("controllers/LocationController.php",{
       id: idToUpdate,
       rtype: rtype,
       setto: setStatusTo
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetLocationByStatus(refreshStatus); 
      }    
)
}

function changeLocationFeatureStatus(idToUpdate, setStatusTo, refreshStatus) {
var rtype="editfstatus";
$.post("controllers/LocationController.php",{
       id: idToUpdate,
       rtype: rtype,
       setto: setStatusTo
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetLocation();
      }    
)
}

function editPicLocation(id){
    var rtype = 'savesession';
    $.post("controllers/LocationController.php",{
       id: id,
       rtype: rtype
       },
       function(response,status){ // Required Callback Function     
        $('#editLocationPicPanel').hide(); 
        $('#editLocationPicPanel').show("slow");
      }    
)
   
    $('html,body,window').animate({scrollTop: $("#editLocationPicPanel").offset().top},'slow');
}

function saveEditLocationImage(){
   var rtype = 'savelocationimage';
    $.post("controllers/LocationController.php",{
       rtype: rtype
       },
       function(response,status){ // Required Callback Function     
        $('#editLocationPicPanel').hide("slow"); 
      }    
)
}


function ajaxGetLocation(){
 var rtype="view";
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype
    },
    url: "controllers/LocationController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
	  var count=0;
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			  count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["place"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["infor"] + "</td>";
        var cont = JSONObject[key]["contextinfor"];
        var text = cont.substr(0, 20) + "...";
        responsesHTML += "<td>"+ text +"</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";   
        if(JSONObject[key]["feature"]==0){
         responsesHTML += "<td onClick='changeLocationFeatureStatus("+JSONObject[key]["id"]+", 1, 0)'><span class='badge badge-secondary' style='cursor:pointer;'>Not Featured</span></td>";
        }
        if(JSONObject[key]["feature"]==1){
         responsesHTML += "<td onClick='changeLocationFeatureStatus("+JSONObject[key]["id"]+", 0, 1)'><span class='badge badge-success' style='background-color:#28A745;cursor:pointer;'>Featured</span></td>";
        }
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeLocationStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeLocationStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        var link = getImageLink();
        responsesHTML += "<td><a href= "+link+JSONObject[key]["image"] +" target='_blank'>View Image</a></td>";
        responsesHTML += "<td onClick='editPicLocation("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Image</td>";
        responsesHTML += "<td onClick='editLocation("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Text</td>";
        responsesHTML += "<td onClick='deleteLocation("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#locationListDiv").html(responsesHTML);
    }
  });
}

function ajaxGetLocationByStatus(status){
   var rtype="viewbystatus";
   if(status==404){
     rtype = "view";
   }
   if(status==0){
     rtype = "viewbystatus";
   }
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype,
      "status": status
    },
    url: "controllers/LocationController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
    var count=0;
    var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
        count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["place"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["infor"] + "</td>";
        var cont = JSONObject[key]["contextinfor"];
        var text = cont.substr(0, 20) + "...";
        responsesHTML += "<td>"+ text +"</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";   
        if(JSONObject[key]["feature"]==0){
         responsesHTML += "<td onClick='changeLocationFeatureStatus("+JSONObject[key]["id"]+", 1, 0)'><span class='badge badge-secondary' style='cursor:pointer;'>Not Featured</span></td>";
        }
        if(JSONObject[key]["feature"]==1){
         responsesHTML += "<td onClick='changeLocationFeatureStatus("+JSONObject[key]["id"]+", 0, 1)'><span class='badge badge-success' style='background-color:#28A745;cursor:pointer;'>Featured</span></td>";
        }
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeLocationStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeLocationStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        var link = getImageLink();
        responsesHTML += "<td><a href= "+link+JSONObject[key]["image"] +" target='_blank'>View Image</a></td>";
        responsesHTML += "<td onClick='editPicLocation("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Image</td>";
        responsesHTML += "<td onClick='editLocation("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Text</td>";
        responsesHTML += "<td onClick='deleteLocation("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#locationListDiv").html(responsesHTML);
    }
  });
}

function ajaxGetLocationByCat(cat){
   var rtype="viewbycat";
   if(cat==404){
     rtype = "view";
   }
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype,
      "cat": cat
    },
    url: "controllers/LocationController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
    var count=0;
    var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
        count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["place"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["infor"] + "</td>";
        var cont = JSONObject[key]["contextinfor"];
        var text = cont.substr(0, 20) + "...";
        responsesHTML += "<td>"+ text +"</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";   
        if(JSONObject[key]["feature"]==0){
         responsesHTML += "<td onClick='changeLocationFeatureStatus("+JSONObject[key]["id"]+", 1, 0)'><span class='badge badge-secondary' style='cursor:pointer;'>Not Featured</span></td>";
        }
        if(JSONObject[key]["feature"]==1){
         responsesHTML += "<td onClick='changeLocationFeatureStatus("+JSONObject[key]["id"]+", 0, 1)'><span class='badge badge-success' style='background-color:#28A745;cursor:pointer;'>Featured</span></td>";
        }
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeLocationStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeLocationStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        var link = getImageLink();
        responsesHTML += "<td><a href= "+link+JSONObject[key]["image"] +" target='_blank'>View Image</a></td>";
        responsesHTML += "<td onClick='editPicLocation("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Image</td>";
        responsesHTML += "<td onClick='editLocation("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Text</td>";
        responsesHTML += "<td onClick='deleteLocation("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#locationListDiv").html(responsesHTML);
    }
  });
}


function ajaxSearchLocation(filterText){
   var rtype="search";
   var filterT=filterText;
   $.ajax({
    type: "POST",
    data: {
    "rtype": rtype,
	  "filterText": filterT
    },
    url: "controllers/LocationController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
	  var count=0;
      // Loop through Object and create peopleHTML
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
        count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["place"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["infor"] + "</td>";
        var cont = JSONObject[key]["contextinfor"];
        var text = cont.substr(0, 20) + "...";
        responsesHTML += "<td>"+ text +"</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";   
        if(JSONObject[key]["feature"]==0){
         responsesHTML += "<td onClick='changeLocationFeatureStatus("+JSONObject[key]["id"]+", 1, 0)'><span class='badge badge-secondary' style='cursor:pointer;'>Not Featured</span></td>";
        }
        if(JSONObject[key]["feature"]==1){
         responsesHTML += "<td onClick='changeLocationFeatureStatus("+JSONObject[key]["id"]+", 0, 1)'><span class='badge badge-success' style='background-color:#28A745;cursor:pointer;'>Featured</span></td>";
        }
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeLocationStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeLocationStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        var link = getImageLink();
        responsesHTML += "<td><a href= "+link+JSONObject[key]["image"] +" target='_blank'>View Image</a></td>";
        responsesHTML += "<td onClick='editPicLocation("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Image</td>";
        responsesHTML += "<td onClick='editLocation("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Text</td>";
        responsesHTML += "<td onClick='deleteLocation("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#locationListDiv").html(responsesHTML);
    }
  });
     //End of ajax
}
$('#searchLocation').keyup(function (e) {
	var selected=$('#searchLocation').val();
	 ajaxSearchLocation(selected);
})
 $("#astatlocation").change(function () {
	   var selected=$('#astatlocation').val();
     ajaxGetLocationByStatus(selected);
 });
$("#catlocation").change(function () {
     var selected=$('#catlocation').val();
     ajaxGetLocationByCat(selected);
 });
//Saving and Updating
function createLocationLocation() {
      var headlines=$('#txtlocationheadlines').val();
      var subtitle=$('#txtlocationsubtitle').val();
      var lat=$('#txtlocationlat').val();
      var lng=$('#txtlocationlng').val();
      var contextinfor=$('#txtlocationcontextinfor').val();
      var category=$('#txtnewscategory').val();
      var rtype="add";  
      if ((/^\s*$/).test(headlines)) {
         Materialize.toast("Enter place name", 3000);      
             }
      else
      if ((/^\s*$/).test(contextinfor)) {
         Materialize.toast("Enter context information.", 3000);           
      } else {
       $.post("controllers/LocationController.php",{
       rtype: rtype,
       headline: headlines,
       category: category,
       subtitle:subtitle,
       contextinfor:contextinfor,
       lat:lat,
       lng:lng
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       if(stat=="OK"){
       Materialize.toast("Location posted successfully", 3000, '',function(){  clearFields(); });  
       }
       else{
       Materialize.toast("Error! " + msg, 6000);      
       }

       }    
       )};
}

function updateLocationLocation() {
      var headlines=$('#txteditlocationheadlines').val();
      var subtitle=$('#txteditlocationsubtitle').val();
      var contextinfor=$('#txteditlocationcontextinfor').val();
      var category=$('#txteditlocationcategory').val();
      var lat=$('#txteditlocationlat').val();
      var lng=$('#txteditlocationlng').val();
      var rtype="update";  
      if ((/^\s*$/).test(headlines)) {
         Materialize.toast("Enter place", 3000);      
             }
      else
      if ((/^\s*$/).test(contextinfor)) {
         Materialize.toast("Enter context information.", 3000);           
      } else {
       $.post("controllers/LocationController.php",{
       rtype: rtype,
       headline: headlines,
       category: category,
       subtitle:subtitle,
       contextinfor:contextinfor,
       lat:lat,
       lng:lng
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       if(stat=="OK"){
       Materialize.toast("Location updated successfully", 3000, '',function(){  clearFields(); });  
       }
       else{
       Materialize.toast("Error! " + msg, 6000);      
       }
       }    
     )};
}