// Users javascript

function deleteShowcase(value, object) {
	var rtype="delete";
	var a = confirm("Are you sure to delete this record?");
     if(a) {
$.post("controllers/ShowcaseController.php",{
       id: value,
	   rtype: rtype
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetShowcase();
       }    
)
}
}

function editShowcase(id){
  var rtype="viewbyid";
   $.ajax({
    type: "POST",
    data: {
    "rtype": rtype,
    "id": id
    },
    url: "controllers/ShowcaseController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
    var count=0;
      // Loop through Object and create peopleHTML
    var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
       if (JSONObject.hasOwnProperty(key)) {
      count++ 
      //Put car details
        $('#txteditshowcasefname').val(JSONObject[key]["film_name"]);
        $('#txteditshowcasefdirector').val(JSONObject[key]["director"]);
        $('#txteditshowcasefactor').val(JSONObject[key]["main_actor"]);
        $('#txteditshowcasefyear').val(JSONObject[key]["year"]);
        $('#txteditshowcaseflink').val(JSONObject[key]["alink"]);
       }         
      }
      loadViewEditShowcase();    
    }
  });
}

function updateFilmShocase(){
      var film_name=$('#txteditshowcasefname').val();
      var director=$('#txteditshowcasefdirector').val();
      var main_actor=$('#txteditshowcasefactor').val();
      var year=$('#txteditshowcasefyear').val();
      var link=$('#txteditshowcaseflink').val();
      var rtype="update";  
      if ((/^\s*$/).test(film_name)) {
         Materialize.toast("Enter film name", 3000);      
             }
      else
      if ((/^\s*$/).test(link)) {
         Materialize.toast("Enter link", 3000);           
      } else {
       $.post("controllers/ShowcaseController.php",{
       rtype:rtype,
       film_name: film_name,
       director: director,
       main_actor: main_actor,
       year:year,
       link:link
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       if(stat=="OK"){
       Materialize.toast("Update successfully", 3000, '',function(){  clearFields(); });  
       }
       else{
       Materialize.toast("Error! " + msg, 6000);      
       }
       }    
     )};
}
function changeShowcaseStatus(idToUpdate, setStatusTo, refreshStatus) {
var rtype="editstatus";
$.post("controllers/ShowcaseController.php",{
       id: idToUpdate,
       rtype: rtype,
       setto: setStatusTo
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetShowcaseByStatus(refreshStatus); 
      }    
)
}


function ajaxGetShowcase(){
 var rtype="view";
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype
    },
    url: "controllers/ShowcaseController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
	  var count=0;
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			  count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["film_name"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["director"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["main_actor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["year"] + "</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";
        responsesHTML += "<td><a href= "+ JSONObject[key]["alink"] +" target='_blank'>Watch</a></td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeShowcaseStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeShowcaseStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        responsesHTML += "<td onClick='editShowcase("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-edit fa-fw' data-toggle='tooltip' title='Edit Info'></i></td>";
        responsesHTML += "<td onClick='deleteShowcase("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#showcaseListDiv").html(responsesHTML);
    }
  });
}

function ajaxGetShowcaseByStatus(status){
   var rtype="viewbystatus";
   if(status==404){
     rtype = "view";
   }
   if(status==0){
     rtype = "viewbystatus";
   }
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype,
      "status": status
    },
    url: "controllers/ShowcaseController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
    var count=0;
    var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
        count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["film_name"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["director"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["main_actor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["year"] + "</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";
        responsesHTML += "<td><a href= "+ JSONObject[key]["alink"] +" target='_blank'>Watch</a></td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeShowcaseStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000;'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeShowcaseStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000;'>Deactivate</td>";
        }
        responsesHTML += "<td onClick='editShowcase("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-edit fa-fw' data-toggle='tooltip' title='Edit Info'></i></td>";
        responsesHTML += "<td onClick='deleteShowcase("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#showcaseListDiv").html(responsesHTML);
    }
  });
}

function ajaxSearchShowcase(filterText){
   var rtype="search";
   var filterT=filterText;
   $.ajax({
    type: "POST",
    data: {
    "rtype": rtype,
	  "filterText": filterT
    },
    url: "controllers/ShowcaseController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
	  var count=0;
      // Loop through Object and create peopleHTML
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			  count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["film_name"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["director"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["main_actor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["year"] + "</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";
        responsesHTML += "<td><a href= "+ JSONObject[key]["alink"] +" target='_blank'>Watch</a></td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeShowcaseStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer;'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeShowcaseStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer;color:#FF0000;'>Deactivate</td>";
        }
        responsesHTML += "<td onClick='editShowcase("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-edit fa-fw' data-toggle='tooltip' title='Edit Info'></i></td>";
        responsesHTML += "<td onClick='deleteShowcase("+JSONObject[key]["id"]+", this)' style='cursor:pointer;color:#008000;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#showcaseListDiv").html(responsesHTML);
    }
  });
     //End of ajax
}
$('#searchShowcase').keyup(function (e) {
	var selected=$('#searchShowcase').val();
	 ajaxSearchShowcase(selected);
})
 $("#astatshowcase").change(function () {
	   var selected=$('#astatshowcase').val();
     ajaxGetShowcaseByStatus(selected);
 });