// Users javascript

function deleteSocial(value, object) {
	var rtype="delete";
	var a = confirm("Are you sure to delete this record?");
     if(a) {
$.post("controllers/SocialController.php",{
       id: value,
	   rtype: rtype
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetSocial();
       }    
)
}
}

function editSocial(id){
  var rtype="viewbyid";
   $.ajax({
    type: "POST",
    data: {
    "rtype": rtype,
    "id": id
    },
    url: "controllers/SocialController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
    var count=0;
      // Loop through Object and create peopleHTML
    var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
       if (JSONObject.hasOwnProperty(key)) {
      count++ 
      //Put car details
        $('#txteditsocialheadlines').val(JSONObject[key]["infor"]);
        $('#txteditsocialsubtitle').val(JSONObject[key]["subtitle"]);
        $('textarea#txteditsocialcontextinfor').val(JSONObject[key]["contextinfor"]);
       }         
      }
      loadViewEditSocial();    
    }
  });
}

function changeSocialStatus(idToUpdate, setStatusTo, refreshStatus) {
var rtype="editstatus";
$.post("controllers/SocialController.php",{
       id: idToUpdate,
       rtype: rtype,
       setto: setStatusTo
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetSocialByStatus(refreshStatus); 
      }    
)
}

function changeSocialFeatureStatus(idToUpdate, setStatusTo, refreshStatus) {
var rtype="editfstatus";
$.post("controllers/SocialController.php",{
       id: idToUpdate,
       rtype: rtype,
       setto: setStatusTo
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       Materialize.toast(msg, 3000);
       ajaxGetSocial();
      }    
)
}

function editPicSocial(id){
    var rtype = 'savesession';
    $.post("controllers/SocialController.php",{
       id: id,
       rtype: rtype
       },
       function(response,status){ // Required Callback Function     
        $('#editSocialPicPanel').hide(); 
        $('#editSocialPicPanel').show("slow");
      }    
)
}

function saveEditSocialImage(){
   var rtype = 'savesocialimage';
    $.post("controllers/SocialController.php",{
       rtype: rtype
       },
       function(response,status){ // Required Callback Function     
        $('#editSocialPicPanel').hide("slow"); 
      }    
)
}


function ajaxGetSocial(){
 var rtype="view";
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype
    },
    url: "controllers/SocialController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
	  var count=0;
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			  count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["infor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["subtitle"] + "</td>";
        var cont = JSONObject[key]["contextinfor"];
        var text = cont.substr(0, 20) + "...";
        responsesHTML += "<td>"+ text +"</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";   
        if(JSONObject[key]["feature"]==0){
         responsesHTML += "<td onClick='changeSocialFeatureStatus("+JSONObject[key]["id"]+", 1, 0)'><span class='badge badge-secondary' style='cursor:pointer;'>Not Featured</span></td>";
        }
        if(JSONObject[key]["feature"]==1){
         responsesHTML += "<td onClick='changeSocialFeatureStatus("+JSONObject[key]["id"]+", 0, 1)'><span class='badge badge-success' style='background-color:#28A745;cursor:pointer;'>Featured</span></td>";
        }
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeSocialStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeSocialStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        var link = getImageLink();
        responsesHTML += "<td><a href= "+link+JSONObject[key]["image"] +" target='_blank'>View Image</a></td>";
        responsesHTML += "<td onClick='editPicSocial("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Image</td>";
        responsesHTML += "<td onClick='editSocial("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Text</td>";
        responsesHTML += "<td onClick='deleteSocial("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#socialListDiv").html(responsesHTML);
    }
  });
}

function ajaxGetSocialByStatus(status){
   var rtype="viewbystatus";
   if(status==404){
     rtype = "view";
   }
   if(status==0){
     rtype = "viewbystatus";
   }
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype,
      "status": status
    },
    url: "controllers/SocialController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
    var count=0;
    var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
        count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["infor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["subtitle"] + "</td>";
        var cont = JSONObject[key]["contextinfor"];
        var text = cont.substr(0, 20) + "...";
        responsesHTML += "<td>"+ text +"</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";   
        if(JSONObject[key]["feature"]==0){
         responsesHTML += "<td onClick='changeSocialFeatureStatus("+JSONObject[key]["id"]+", 1, 0)'><span class='badge badge-secondary' style='cursor:pointer;'>Not Featured</span></td>";
        }
        if(JSONObject[key]["feature"]==1){
         responsesHTML += "<td onClick='changeSocialFeatureStatus("+JSONObject[key]["id"]+", 0, 1)'><span class='badge badge-success' style='background-color:#28A745;cursor:pointer;'>Featured</span></td>";
        }
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeSocialStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeSocialStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        var link = getImageLink();
        responsesHTML += "<td><a href= "+link+JSONObject[key]["image"] +" target='_blank'>View Image</a></td>";
        responsesHTML += "<td onClick='editPicSocial("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Image</td>";
        responsesHTML += "<td onClick='editSocial("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Text</td>";
        responsesHTML += "<td onClick='deleteSocial("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#socialListDiv").html(responsesHTML);
    }
  });
}

function ajaxGetSocialByCat(cat){
   var rtype="viewbycat";
   if(cat==404){
     rtype = "view";
   }
   $.ajax({
    type: "POST",
    data: {
      "rtype": rtype,
      "cat": cat
    },
    url: "controllers/SocialController.php",
    dataType: "json",
    success: function(JSONArray) {
    var responsesHTML = "";
    var count=0;
    var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
        count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["infor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["subtitle"] + "</td>";
        var cont = JSONObject[key]["contextinfor"];
        var text = cont.substr(0, 20) + "...";
        responsesHTML += "<td>"+ text +"</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";   
        if(JSONObject[key]["feature"]==0){
         responsesHTML += "<td onClick='changeSocialFeatureStatus("+JSONObject[key]["id"]+", 1, 0)'><span class='badge badge-secondary' style='cursor:pointer;'>Not Featured</span></td>";
        }
        if(JSONObject[key]["feature"]==1){
         responsesHTML += "<td onClick='changeSocialFeatureStatus("+JSONObject[key]["id"]+", 0, 1)'><span class='badge badge-success' style='background-color:#28A745;cursor:pointer;'>Featured</span></td>";
        }
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeSocialStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeSocialStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        var link = getImageLink();
        responsesHTML += "<td><a href= "+link+JSONObject[key]["image"] +" target='_blank'>View Image</a></td>";
        responsesHTML += "<td onClick='editPicSocial("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Image</td>";
        responsesHTML += "<td onClick='editSocial("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Text</td>";
        responsesHTML += "<td onClick='deleteSocial("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#socialListDiv").html(responsesHTML);
    }
  });
}


function ajaxSearchSocial(filterText){
   var rtype="search";
   var filterT=filterText;
   $.ajax({
    type: "POST",
    data: {
    "rtype": rtype,
	  "filterText": filterT
    },
    url: "controllers/SocialController.php",
    dataType: "json",
    success: function(JSONArray) {
      var responsesHTML = "";
	  var count=0;
      // Loop through Object and create peopleHTML
	  var JSONObject=JSONArray["records"];
      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
			  count++
        responsesHTML += "<tr>";
        responsesHTML += "<td>" + count + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["infor"] + "</td>";
        responsesHTML += "<td>" + JSONObject[key]["subtitle"] + "</td>";
        var cont = JSONObject[key]["contextinfor"];
        var text = cont.substr(0, 20) + "...";
        responsesHTML += "<td>"+ text +"</td>";
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td style='color: #F58E25;'>Inactive</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td style='color: #008000;'>Active</td>";
        }
        responsesHTML += "<td>" + JSONObject[key]["created"] + "</td>";   
        if(JSONObject[key]["feature"]==0){
         responsesHTML += "<td onClick='changeSocialFeatureStatus("+JSONObject[key]["id"]+", 1, 0)'><span class='badge badge-secondary' style='cursor:pointer;'>Not Featured</span></td>";
        }
        if(JSONObject[key]["feature"]==1){
         responsesHTML += "<td onClick='changeSocialFeatureStatus("+JSONObject[key]["id"]+", 0, 1)'><span class='badge badge-success' style='background-color:#28A745;cursor:pointer;'>Featured</span></td>";
        }
        if(JSONObject[key]["status"]==0){
         responsesHTML += "<td onClick='changeSocialStatus("+JSONObject[key]["id"]+", 1, 0)' style='cursor:pointer; color:#008000'>Activate</td>";
        }
        if(JSONObject[key]["status"]==1){
         responsesHTML += "<td onClick='changeSocialStatus("+JSONObject[key]["id"]+", 0, 1)' style='cursor:pointer; color:#FF0000'>Deactivate</td>";
        }
        var link = getImageLink();
        responsesHTML += "<td><a href= "+link+JSONObject[key]["image"] +" target='_blank'>View Image</a></td>";
        responsesHTML += "<td onClick='editPicSocial("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Image</td>";
        responsesHTML += "<td onClick='editSocial("+JSONObject[key]["id"]+", this)' style='cursor:pointer; color:#e85a21;'>Edit Text</td>";
        responsesHTML += "<td onClick='deleteSocial("+JSONObject[key]["id"]+", this)' style='cursor:pointer;'><i class='fa fa-trash-o fa-fw' data-toggle='tooltip' title='Delete Info'></i></td>";
        responsesHTML += "</tr>";
       }         
      }
      $("#socialListDiv").html(responsesHTML);
    }
  });
     //End of ajax
}
$('#searchSocial').keyup(function (e) {
	var selected=$('#searchSocial').val();
	 ajaxSearchSocial(selected);
})
 $("#astatsocial").change(function () {
	   var selected=$('#astatsocial').val();
     ajaxGetSocialByStatus(selected);
 });
$("#catsocial").change(function () {
     var selected=$('#catsocial').val();
     ajaxGetSocialByCat(selected);
 });
//Saving and Updating
function createSocialSocial() {
      var headlines=$('#txtsocialheadlines').val();
      var subtitle=$('#txtsocialsubtitle').val();
      var contextinfor=$('#txtsocialcontextinfor').val();
      var category=$('#txtsocialcategory').val();
      var rtype="add";  
      if ((/^\s*$/).test(headlines)) {
         Materialize.toast("Enter headlines", 3000);      
             }
      else
      if ((/^\s*$/).test(contextinfor)) {
         Materialize.toast("Enter context information.", 3000);           
      } else {
       $.post("controllers/SocialController.php",{
       rtype: rtype,
       headline: headlines,
       category: category,
       subtitle:subtitle,
       contextinfor:contextinfor
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       if(stat=="OK"){
       Materialize.toast("Information posted successfully", 3000, '',function(){  clearFields(); });  
       }
       else{
       Materialize.toast("Error! " + msg, 6000);      
       }

       }    
       )};
}

function updateSocialSocial() {
      var headlines=$('#txteditsocialheadlines').val();
      var subtitle=$('#txteditsocialsubtitle').val();
      var contextinfor=$('#txteditsocialcontextinfor').val();
      var category=$('#txteditsocialcategory').val();
      var rtype="update";  
      if ((/^\s*$/).test(headlines)) {
         Materialize.toast("Enter headlines", 3000);      
             }
      else
      if ((/^\s*$/).test(contextinfor)) {
         Materialize.toast("Enter context information.", 3000);           
      } else {
       $.post("controllers/SocialController.php",{
       rtype: rtype,
       headline: headlines,
       category: category,
       subtitle:subtitle,
       contextinfor:contextinfor
       },
       function(response,status){ // Required Callback Function      
       var stat=response["stat"];
       var msg=response["msg"];
       if(stat=="OK"){
       Materialize.toast("Information updated successfully", 3000, '',function(){  clearFields(); });  
       }
       else{
       Materialize.toast("Error! " + msg, 6000);      
       }
       }    
     )};
}