<?php session_start();?>
<!DOCTYPE html>

<html>
<head>
	<title>Maps</title>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=geometry,places&key=AIzaSyAJKXWHcOeBl9hE2AIscFDP6AKzJb1ItY4"></script>		<!-- Google Maps API -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>	<!-- Google CDN for jQuery -->
    <script src = "../libs/js/angular.min.js"></script>
    <script src = "../libs/js/lolliclock.js"></script>
<script src = "../app/controllers/angular_map_controller.js"></script>
  <!-- Compiled and minified CSS -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
 <link href="../libs/css/truckinfor.css" rel="stylesheet">
  <link href="../libs/css/lolliclock.css" rel="stylesheet">
 <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
  <!-- Compiled and minified JavaScript --> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
 
	  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<style>
@import url(http://fonts.googleapis.com/css?family=Baloo+Bhaina);
@import url(http://fonts.googleapis.com/css?family=Tangerine);
@font-face {
  font-family: 'Material Icons';
  font-style: normal;
  font-weight: 40px;
  src: local('Material Icons'), local('MaterialIcons-Regular'), url(../libs/css/materialize/fonts/icons/Material-Font.woff2) format('woff2');
}
#toast-container {
  top: auto !important;
  right: auto !important;
  bottom: 50%;
  left:40%;
}
.notifyuser {
 font-family: Baloo Bhaina;
 font-style: normal;
}
.material-icons {
  font-family: 'Material Icons';
  font-weight: normal;
  font-style: normal;
  font-size: 24px;
  line-height: 1;
  letter-spacing: normal;
  text-transform: none;
  display: inline-block;
  white-space: nowrap;
  word-wrap: normal;
  direction: ltr;
  -webkit-font-feature-settings: 'liga';
  -webkit-font-smoothing: antialiased;
}
.width-30-pct{
    width:30%;
}
.picker { outline: none; } /* Optional feature */
/* Header - Day of the week */
.picker__weekday-display { background-color: rgb(236, 30, 30);
    font-weight: 600;}
/* Body - Today info */
.picker__date-display { background-color: rgb(121,134,203); }
/* Buttons of actions */
.picker__nav--prev:hover { background-color: #fff3e0; }
.picker__nav--next:hover { background-color: #fff3e0; }
.picker__nav--prev:before {
  border-left: 0;
  border-right: 0.75em solid #0F74BD;
}
.picker__nav--next:before {
  border-right: 0;
  border-left: 0.75em solid #0F74BD;
}
.picker__today { color: #15F384; }
.picker__clear { /* If you want to customize */ }
.picker__close { color: #15F384; }
.btn-flat.picker__today:active { background-color: #fff3e0; }
.btn-flat.picker__close:active { background-color: #fff3e0; }
.btn-flat.picker__clear:active { background-color: #fff3e0; }
/* Select months of the year */
.picker__select--month.browser-default {
  display: inline;
  background-color: #FFFFFF;
  width: 34%;
  border: none;
  outline: none;
}
/* Every month of the year except: today */
.picker__day { /* If you want to customize */ }
.picker__day:hover { /* If you want to customize */ }
.picker__day--infocus:hover { color: #f57c00; }
.picker__day--selected, .picker__day--selected:hover, .picker--focused .picker__day--selected { 
    color: #F5F4F3;
    font-weight: 700;
}
.picker__day.picker__day--infocus.picker__day--selected.picker__day--highlighted { background-color: rgb(255,64,129); }
/* Today */
.picker__day.picker__day--infocus.picker__day--today.picker__day--selected.picker__day--highlighted { background-color: rgb(255,64,129); }
.picker__day.picker__day--today 
{ color: rgb(15, 50, 234);
  font-weight: 700; }
.container {
  margin: 0 auto;
  max-width: 1280px;
  width: 90%;
}
@media only screen and (min-width: 601px)
  .container {
    width: 85%;
}
@media only screen and (min-width: 993px)
  .container {
    width: 90%;
}
	.card-panel{
		    padding: 5px;
		    font-size: 13px;
	}
 
.text-align-center{
    text-align:center;
}
 
.margin-bottom-1em{
    margin-bottom:1em;
}
</style>
	<script>
	var map;	// Google map object
	
	// Initialize and display a google map
	$(function() {  
	   var line = [];
	    $("#hiddedTextFields").hide();
	    $("#address3").hide();
		$("#address4").hide();
		$("#two").hide();
		$('.pick-a-time').lolliclock({ autoclose: false });
		$('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 1 // Creates a dropdown of 15 years to control year
         });
		$('select').material_select();
		// Create a Google coordinate object for where to initially center the map
		
		var latlng = new google.maps.LatLng( -25.9991795, 28.126292700000022 );	// Midrand JHB
		
		// Map options for how to display the Google map
		var mapOptions = {
			zoom: 12, 
			center: latlng, 
			mapTypeId: google.maps.MapTypeId.ROADMAP,
        
			};	
		// Show the Google map in the div with the attribute id 'map-canvas'.
		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);	
   
	} );

  jQuery(window).load(function () {
   $("#step2").hide();
   $("#step3").hide();
  });

	function ShowLocation( latlng, address, addr_type )
	{

		// Center the map at the specified location
		map.setCenter( latlng );
		// Set the zoom level according to the address level of detail the user specified
		var zoom = 12;
		switch ( addr_type )
		{
		case "administrative_area_level_1"	: zoom = 6; break;		// user specified a state
		case "locality"						: zoom = 10; break;		// user specified a city/town
		case "street_address"				: zoom = 15; break;		// user specified a street address
		}
		map.setZoom( zoom );
		var marker = new google.maps.Marker( { 
			position: latlng,     
			map: map,      
			title: address
		});

	}

	function DrawRoute(){
	           var directionsDisplay = new google.maps.DirectionsRenderer;
               var directionsService = new google.maps.DirectionsService;
               directionsDisplay.setMap(map);
               calculateAndDisplayRoute(directionsService, directionsDisplay);
	}

	function DrawRoute2(){
	           var directionsDisplay = new google.maps.DirectionsRenderer;
               var directionsService = new google.maps.DirectionsService;
               directionsDisplay.setMap(map);
               calculateAndDisplayRoute2(directionsService, directionsDisplay);
	}
	
	function DrawRoute3(){
	           var directionsDisplay = new google.maps.DirectionsRenderer;
               var directionsService = new google.maps.DirectionsService;
               directionsDisplay.setMap(map);
               calculateAndDisplayRoute3(directionsService, directionsDisplay);
	}
	function calculateAndDisplayRoute(directionsService, directionsDisplay) {
		var lat1 = parseFloat(document.getElementById( "lat1" ).value);
		var lng1 = parseFloat(document.getElementById( "lng1" ).value);
	    var lat2 = parseFloat(document.getElementById( "lat2" ).value);
		var lng2 = parseFloat(document.getElementById( "lng2" ).value);

        var selectedMode = "DRIVING";
        directionsService.route({
          origin: {lat: lat1, lng: lng1},
          destination: {lat: lat2, lng: lng2},  
          travelMode: google.maps.TravelMode[selectedMode]
        }, function(response, status) {
          if (status == 'OK') {
            directionsDisplay.setDirections(response);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
    }

    //Claculate distance and time
    function GetRoute(source, destination, rtype) {
    //*********DIRECTIONS AND ROUTE**********************//
   //var source = document.getElementById("address").value;
    var adr = document.getElementById("address3").value;
    var distance, duration;
    //*********DISTANCE AND DURATION**********************//
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix({
        origins: [source],
        destinations: [destination],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
    }, function (response, status) {
        if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
            distance = response.rows[0].elements[0].distance.text;
            duration = response.rows[0].elements[0].duration.text;
              if(rtype===0){
              Materialize.toast("Results: Distance= " + distance + " and Time:  " + duration, 5000);
              if(adr===''){
              $('<font color=#29B2A5>' + distance+ '</font>').appendTo('#distanceFound');
              $('<font color=#29B2A5>' + duration+ '</font>').appendTo('#travelTime');
              }
              else{
              $('<font color=#FF0000>+</font><font color=#29B2A5>' + distance+ '</font>').appendTo('#distanceFound');
              $('<font color=#FF0000>+</font><font color=#29B2A5>' + duration+ '</font>').appendTo('#travelTime');
              }
              }
        } else {
            alert("Failed to calculate distance. Reload browser and try again");
        }
    });
    }
    //End of calculate distance and time
    function calculateAndDisplayRoute2(directionsService, directionsDisplay) {
		var lat3 = parseFloat(document.getElementById( "lat3" ).value);
		var lng3 = parseFloat(document.getElementById( "lng3" ).value);
	    var lat2 = parseFloat(document.getElementById( "lat2" ).value);
		var lng2 = parseFloat(document.getElementById( "lng2" ).value);
		var lat1 = parseFloat(document.getElementById( "lat1" ).value);
		var lng1 = parseFloat(document.getElementById( "lng1" ).value);
		var adr2 = document.getElementById( "address2" ).value;
        var selectedMode = "DRIVING";
        //Way points
        var items = [adr2];
        var waypoints = [];
        for (var i = 0; i < items.length; i++) {
        var address = items[i];
        if (address !== "") {
        waypoints.push({
            location: address,
            stopover: true
        });
        }
        }

        //End of way points
        directionsService.route({
          origin: {lat: lat1, lng: lng1},
          destination: {lat: lat3, lng: lng3},  
          waypoints: waypoints,
          optimizeWaypoints: false,
          travelMode: google.maps.TravelMode[selectedMode]
        }, function(response, status) {
          if (status == 'OK') {
            directionsDisplay.setDirections(response);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
    }


    function calculateAndDisplayRoute3(directionsService, directionsDisplay) {
    	var lat4 = parseFloat(document.getElementById( "lat4" ).value);
		var lng4 = parseFloat(document.getElementById( "lng4" ).value);
		var lat3 = parseFloat(document.getElementById( "lat3" ).value);
		var lng3 = parseFloat(document.getElementById( "lng3" ).value);
	    var lat2 = parseFloat(document.getElementById( "lat2" ).value);
		var lng2 = parseFloat(document.getElementById( "lng2" ).value);
		var lat1 = parseFloat(document.getElementById( "lat1" ).value);
		var lng1 = parseFloat(document.getElementById( "lng1" ).value);
		var adr2 = document.getElementById( "address2" ).value;
		var adr3 = document.getElementById( "address3" ).value;
        var selectedMode = "DRIVING";
        //Way points
        var items = [adr2, adr3];
        var waypoints = [];
        for (var i = 0; i < items.length; i++) {
        var address = items[i];
        if (address !== "") {
        waypoints.push({
            location: address,
            stopover: true
        });
        }
        }

        //End of way points
        directionsService.route({
          origin: {lat: lat1, lng: lng1},
          destination: {lat: lat4, lng: lng4},  
          waypoints: waypoints,
          optimizeWaypoints: false,
          travelMode: google.maps.TravelMode[selectedMode]
        }, function(response, status) {
          if (status == 'OK') {
            directionsDisplay.setDirections(response);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
    }

    

	</script>
	<style>
	/* style settings for Google map */
	#map-canvas
	{
		width : 500px; 	/* map width */
		height: 500px;	/* map height */
	}
	</style>
</head>
<body ng-app="mapModule">
 
 <script type="text/javascript">
 var IsplaceChange1 = false;
 var IsplaceChange2 = false;
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('address'));
			var places2 = new google.maps.places.Autocomplete(document.getElementById('address2'));
			var places3 = new google.maps.places.Autocomplete(document.getElementById('address3'));
			var places4 = new google.maps.places.Autocomplete(document.getElementById('address4'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                IsplaceChange1 = true;
			   //plot marker
			   var geocoder = new google.maps.Geocoder();    // instantiate a geocoder object		
			// Get the user's inputted address
			var address = document.getElementById( "address" ).value;
			 SetLngLongPlot1(latitude, longitude);
			// Make asynchronous call to Google geocoding API

      //Find close trucks
      ajaxGetMyTrucks();
      //End of close trucks
			geocoder.geocode( { 'address': address }, function(results, status) {
				var addr_type = results[0].types[0];	// type of address inputted that was geocoded
				if ( status == google.maps.GeocoderStatus.OK ){				   
					ShowLocation( results[0].geometry.location, address, addr_type );}
				else     
					alert("Geocode was not successful for the following reason: " + status);        
			});  

        });
        $("#address").keydown(function () {
            IsplaceChange1 = false;
        });
        $("#address").keyup(function () {
            var ts=checkTruckSelected();
            if (ts==false) {           
            document.getElementById('address').value = '';
            }
        });
			
			//Place two
			 google.maps.event.addListener(places2, 'place_changed', function () {
                var place2 = places2.getPlace();
                var address2 = place2.formatted_address;
                var latitude2 = place2.geometry.location.lat();
                var longitude2 = place2.geometry.location.lng();
                var geocoder = new google.maps.Geocoder();   
                IsplaceChange2=true;
                 // instantiate a geocoder object
				// Get the user's inputted address
			// Make asynchronous call to Google geocoding API
          updateProviderView();
          SetLngLongPlot2(latitude2, longitude2); 
          var source = document.getElementById("address").value;
          var destination = document.getElementById("address2").value;		    
          GetRoute(source, destination, 0);
			    	
			   geocoder.geocode( { 'address': address2 }, function(results, status) {
				var addr_type = results[0].types[0];	// type of address inputted that was geocoded
				if ( status == google.maps.GeocoderStatus.OK ){
					DrawRoute();
                   // ShowLocation( results[0].geometry.location, address2, addr_type );	
				}
				else     
					alert("Geocode was not successful for the following reason: " + status);        
			});

            });
        $("#address2").keydown(function () {
            IsplaceChange2 = false;
        });
        $("#address2").keyup(function () {
            var ts=checkTruckSelected();
            if (ts==false) {           
            document.getElementById('address2').value = '';
            }
        });
			
			//Place 3
			 google.maps.event.addListener(places3, 'place_changed', function () {
                var place3 = places3.getPlace();
                var address3 = place3.formatted_address;
                var latitude3 = place3.geometry.location.lat();
                var longitude3 = place3.geometry.location.lng();
                var geocoder = new google.maps.Geocoder();    // instantiate a geocoder object
				// Get the user's inputted address
			// Make asynchronous call to Google geocoding API
			 SetLngLongPlot3(latitude3, longitude3);
       var source = document.getElementById("address2").value;
       var destination = document.getElementById("address3").value;
       GetRoute(source, destination, 0);
 
			 geocoder.geocode( { 'address': address3 }, function(results, status) {
				var addr_type = results[0].types[0];	// type of address inputted that was geocoded
				if ( status == google.maps.GeocoderStatus.OK ){
					DrawRoute2();				  
				}
				else     
					alert("Geocode was not successful for the following reason: " + status);        
			});

            });
			
			//Place 4
			 google.maps.event.addListener(places4, 'place_changed', function () {
                var place4 = places4.getPlace();
                var address4 = place4.formatted_address;
                var latitude4 = place4.geometry.location.lat();
                var longitude4 = place4.geometry.location.lng();
                var geocoder = new google.maps.Geocoder();    // instantiate a geocoder object
				// Get the user's inputted address
			// Make asynchronous call to Google geocoding API
			 SetLngLongPlot4(latitude4, longitude4);
       var source = document.getElementById("address3").value;
       var destination = document.getElementById("address4").value;
       GetRoute(source, destination, 0);
			 geocoder.geocode( { 'address': address4 }, function(results, status) {
				var addr_type = results[0].types[0];	// type of address inputted that was geocoded
				if ( status == google.maps.GeocoderStatus.OK ){
				  DrawRoute3();		  
				}
				else     
				  alert("Geocode was not successful for the following reason: " + status);        
			});

            });
        });
    function setValue(value){
     $('#foundProviders').val(value);
    }
    function getValue(){
    var val= $('#foundProviders').val();
    return val;
    }
/*
    function getCloseTrucks(latitude, longitude){
    $.post("ajaxCloseTrucks.php",{
      flat: latitude ,
      flng: longitude
    },
    function(response,status){ // Required Callback Function       
       var found=response["found"]; 
       $('#foundProviders').val(found);
       $("<span class='badge green'><font color='#FFFFFF'>" +found+ "</font></span>").appendTo("#providersFound");
      }
     )
    }*/
    function updateProviderView(){
      $.post("get_provider_session.php",{
          id:1
        },
      function(response,status){ // Required Callback Function       
       var msg=response["msg"]; 
       Materialize.toast("Providers found: ["+msg+"]", 6000); 
       $('#foundProviders').val(msg);
       $('#providersFound').html("<span class='text notifyuser'>Available Providers:</span><span class='badge green'><font color='#FFFFFF'>"+ msg + "</font></span>"); 
      }
     )
    }

    function calculateDistance(pid,padr,param){
      var departure=document.getElementById("address").value;
      var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix({
        origins: [departure],
        destinations: [padr],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
    }, function (response, status) {
        if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
           var distance = response.rows[0].elements[0].distance.text;
           var res = distance.replace(",", "");
           var qLimit=parseFloat(param);
           var d=parseFloat(res);
            if(d<=qLimit){
             //Make ajax request to save session//         
            $.post("saveProvidersFoundSession.php",{pid: pid});
             //End of ajax request//       
            }   
        } else {
            alert("Failed to calculate distance. Reload browser and try again");
        }   
    });

    }
    function ajaxGetMyTrucks(){
   //Some jax here
    $.ajax({
    type: "POST",
    data: {
      "stat": 1
    },
    url: "ajaxCloseTrucks2.php",
    dataType: "json",
    success: function(JSONObject) { 
      // Loop through Object and create peopleHTML
      for (var key in JSONObject) {        
        if (JSONObject.hasOwnProperty(key)) {
        var param= JSONObject[key]["param"];
        var padr= JSONObject[key]["padr"];  
        var pid= JSONObject[key]["pid"];  
        //*********DISTANCE AND DURATION*************//
        calculateDistance(pid,padr,param);
        //End of distance calculation
       }    
      }
   //   $("#btnRefreshProvider").trigger("click");   
    }
  })
   
 };

  function stepOne(){
       var c =checkTruckSelected();
       if(c==false){
       return false;      
       }
       else{
	   $("#loadcontainer").animate({height:'660px'}, 350);
       $("#step1").hide("slow");
       $("#step2").show("slow");
	   
       }
  }
  function stepTwo(){
     var t = checkTruckSelected();
       if(t==false){
         return false;
       }
       else
       if ((/^\s*$/).test(address)) {
         Materialize.toast("Enter departure address please", 3000); 
          return false;     
             }
      else
        if(IsplaceChange1==false){
           Materialize.toast("Type and select departure address", 3000); 
          return false; 
        }
       else
       if ((/^\s*$/).test(address2)) {
         Materialize.toast("Enter destination address please", 3000); 
          return false;          
             }
             else
        if(IsplaceChange2==false){
           Materialize.toast("Type and select destination address", 3000); 
          return false; 
        }
       else
       if(parseInt(found)===0){
         Materialize.toast("Sorry your request cannot be processed. There are no providers.", 3000);
          return false;  
       }
       else
      {
       $("#step2").hide("slow");
       $("#step3").show("slow");
      }
  }
	  function addLine(x) {
        x.setMap(map);
      }

      function removeLine(x) {
        x.setMap(null);
      }
		function SetLngLongPlot1(v1, v2) {
           document.getElementById( "lat1" ).value = v1;
		   document.getElementById( "lng1" ).value = v2;
        }
		function SetLngLongPlot2(v1, v2) {
           document.getElementById( "lat2" ).value = v1;
		   document.getElementById( "lng2" ).value = v2;
        }
		function SetLngLongPlot3(v1, v2) {
           document.getElementById( "lat3" ).value = v1;
		   document.getElementById( "lng3" ).value = v2;
        }
		function SetLngLongPlot4(v1, v2) {
           document.getElementById( "lat4" ).value = v1;
		   document.getElementById( "lng4" ).value = v2;
        }
		function getValues() {
			var lat1 = document.getElementById( "lat1" ).value;
			var lng1 = document.getElementById( "lng1" ).value;
			var lat2 = document.getElementById( "lat2" ).value;
			var lng2 = document.getElementById( "lng2" ).value;
        }
		function openHelp(value, object) {
			var message = document.getElementById('msgContent');
            var msgframe = document.getElementById('myMessage');
			var msgtitle = document.getElementById('msgTitle');
			message.textContent = value;
			msgtitle.textContent = "About Truck";
            msgframe.style.display = "block";
        }
		function viewMore() {
		    $("#one").hide();
		    $("#two").show();
			//alert("View More");
        }
    function checkTruckSelected(){
       var r = document.getElementsByName("struck");
        var c = -1
        for(var i=0; i < r.length; i++){
        if(r[i].checked) {
         c = i; 
        }
       }
       if (c == -1){
         Materialize.toast("Select truck please", 3000); 
         return false;
       }
    }
		function viewLess() {
		    $("#two").hide();
		  	$("#one").show();
			//alert("View Less");
        }
        function closeHelp() {
			var msgframe = document.getElementById('myMessage');
            msgframe.style.display = "none";
        }
      function validate_truck_request() {
      var ttype=$('input[name="struck"]:checked', '#myForm').val();
      var fname=$('#fname').val();
      var email=$('#email').val();
      var cell=$('#cell').val();
      var ltype=$('#ltype').val();
      var communication=$('#communication').val();
      var expire=$('#expire').val();
      var expiretime=$('#expiretime').val();
      var otherinfor=$('#otherinfor').val();
      var address=$('#address').val();
      var address4=$('#address4').val();
      var address2=$('#address2').val();
      var address3=$('#address3').val();
      var pickupd=$('#pickupd').val();
      var pickupt=$('#pickupt').val();
      var found=$('#foundProviders').val();
       var t = checkTruckSelected();
       if(t==false){
         return false;
       }
       else
       if ((/^\s*$/).test(address)) {
         Materialize.toast("Enter departure address please", 3000); 
          return false;     
             }
      else
        if(IsplaceChange1==false){
           Materialize.toast("Type and select departure address", 3000); 
          return false; 
        }
       else
       if ((/^\s*$/).test(address2)) {
         Materialize.toast("Enter destination address please", 3000); 
          return false;          
             }
             else
        if(IsplaceChange2==false){
           Materialize.toast("Type and select destination address", 3000); 
          return false; 
        }
       else
       if(parseInt(found)===0){
         Materialize.toast("Sorry your request cannot be processed. There are no providers.", 3000);
          return false;  
       }
       else
       if ((/^\s*$/).test(fname)) {
        Materialize.toast("Error! Enter your name please: ", 3000);   
         return false; 
             }
       else
       if ((/^\s*$/).test(email)) {
        Materialize.toast("Enter your email", 3000);
         return false;     
             }
       else
       if ((/^\s*$/).test(cell)) {
              Materialize.toast("Error! Enter your cell", 3000);  
               return false;     
             }
       else
       if ((/^\s*$/).test(expire)) {
              Materialize.toast("Error! Enter expire date of request", 3000);   
               return false;    
             }
       else
       if ((/^\s*$/).test(expiretime)) {
            Materialize.toast("Error! Enter expire time of request", 3000);      
             return false;
       }
       else{
        return true;
       }
      }
		function sendRequest() {
      var ttype=$('input[name="struck"]:checked', '#myForm').val();
      var fname=$('#fname').val();
      var email=$('#email').val();
      var cell=$('#cell').val();
      var ltype=$('#ltype').val();
      var communication=$('#communication').val();
      var expire=$('#expire').val();
      var expiretime=$('#expiretime').val();
      var otherinfor=$('#otherinfor').val();
      var address=$('#address').val();
      var address4=$('#address4').val();
      var address2=$('#address2').val();
      var address3=$('#address3').val();
      var pickupd=$('#pickupd').val();
      var pickupt=$('#pickupt').val();
      var found=$('#foundProviders').val();
       var t = checkTruckSelected();
       if(t==false){
         return false;
       }
       else
       if ((/^\s*$/).test(address)) {
         Materialize.toast("Enter departure address please", 3000); 
          return false;     
             }
      else
        if(IsplaceChange1==false){
           Materialize.toast("Type and select departure address", 3000); 
          return false; 
        }
       else
       if ((/^\s*$/).test(address2)) {
         Materialize.toast("Enter destination address please", 3000); 
          return false;          
             }
             else
        if(IsplaceChange2==false){
           Materialize.toast("Type and select destination address", 3000); 
          return false; 
        }
       else
       if(parseInt(found)===0){
         Materialize.toast("Sorry your request cannot be processed. There are no providers.", 3000);
          return false;  
       }
       else
       if ((/^\s*$/).test(fname)) {
        Materialize.toast("Error! Enter your name please: ", 3000);   
         return false; 
             }
       else
       if ((/^\s*$/).test(email)) {
        Materialize.toast("Enter your email", 3000);
         return false;     
             }
       else
       if ((/^\s*$/).test(cell)) {
              Materialize.toast("Error! Enter your cell", 3000);  
               return false;     
             }
       else
       if ((/^\s*$/).test(expire)) {
              Materialize.toast("Error! Enter expire date of request", 3000);   
               return false;    
             }
       else
       if ((/^\s*$/).test(expiretime)) {
            Materialize.toast("Error! Enter expire time of request", 3000);      
             return false;
       }
       else{
			 $.post("saveInquiry.php",{
			 ttype: ttype,
			 fname: fname,
			 email: email,
			 cell: cell,
			 ltype: ltype,
			 communication: communication,
			 expire: expire,
			 expiretime: expiretime,
			 pickupd: pickupd,
			 pickupt: pickupt,
			 otherinfor: otherinfor,
			 address: address,
			 address2: address2,
			 address3: address3,
			 address4: address4
			 },
			 function(response,status){ // Required Callback Function  		 
			 var stat=response["stat"];
			 var msg=response["msg"];
			 if(stat=="pass"){
			 Materialize.toast("Your request for " + ttype + " has been sent." + msg, 6000);      
			 }
			 else{
			 Materialize.toast("Error! " + msg, 6000);      
			 }
			 }		
			 );
      }
		}
    // When the user clicks anywhere outside of the modal, close it
        window.onclick = function (event) {
	    var msgframe = document.getElementById('myMessage');
        if (event.target == msgframe) {
			var msgframe = document.getElementById('myMessage');
            msgframe.style.display = "none";
        }
        }
    </script>
    <!-- The Modal -->
       <!-- The Modal -->
       <?php
       include("../config/db_con2.php");
       ?>
<div id="myMessage" class="msg">
    <!-- MsgBox content -->
    <div class="msg-content">
        <div class="msg-header">
            <h5><div id="msgTitle">About Truck</div></h5>
            <hr style="width:99%; color:#900;" />
        </div>
        <br />
        <div class="msg-body" id="msgContent">

        </div>
        <p>
        </p>
    </div>
</div>

    <!--Put Cards Here -->
     <div class="container" id="step1" style="width: 98%;">
     <form id="myForm">
    <div class="row">
    <div class="col s12 m3">
      <div class="card hoverable">
       <div class="card-image">
          <img src="../images/1-ton1.jpg" style="padding:10px; border-radius:25px; height:165px;">
        </div>
        <div class="card-image">
          <img src="../images/1-ton1.jpg" style="padding:10px; border-radius:25px; height:165px;">
         
     <a class="btn-floating halfway-fab waves-effect waves-light blue" onClick="openHelp('A 1/2 ton truck is a pickup truck with a certain curb weight and payload capacity', this)"><i class="material-icons">help</i></a>
        </div>
         <span class="card-title"><center><p style="padding-left:10px;">
    <input class="with-gap" name="struck" type="radio" id="t1" value="1 Tonne Truck"/>
    <label for="t1"><b><font color="#000000">1 TON</font></b></label>
  </p></center></span>
      </div>
    </div>
    
    
    <div class="col s12 m3">
      <div class="card hoverable">
       <div class="card-image">
          <img src="../images/2-ton1.jpg" style="padding:10px; border-radius:25px; height:165px;">
        </div>
        <div class="card-image">
          <img src="../images/2-ton1.jpg" style="padding:10px; border-radius:25px; height:165px;">
         
     <a class="btn-floating halfway-fab waves-effect waves-light blue" onClick="openHelp('A 2/3 ton truck is a pickup truck with a certain curb weight and payload capacity', this)"><i class="material-icons">help</i></a>
        </div>
         <span class="card-title"><center><p style="padding-left:10px;">
    <input class="with-gap" name="struck" type="radio" id="t2" value="2-3 Tonne Truck"/>
    <label for="t2"><b><font color="#000000">2-3 TON</font></b></label>
  </p></center></span>
      </div>
    </div>
	
	   <div class="col s12 m3">
      <div class="card hoverable">
       <div class="card-image">
          <img src="../images/bakkie.jpg" style="padding:10px; border-radius:25px; height:165px;">
        </div>
        <div class="card-image">
          <img src="../images/bakkie.jpg" style="padding:10px; border-radius:25px; height:165px;">
         
     <a class="btn-floating halfway-fab waves-effect waves-light blue" onClick="openHelp('A Bakkie is a pickup truck with a certain curb weight and payload capacity', this)"><i class="material-icons">help</i></a>
        </div>
         <span class="card-title"><center><p style="padding-left:10px;">
    <input class="with-gap" name="struck" type="radio" id="bk" value="Bakkie Truck"/>
    <label for="bk"><b><font color="#000000">BAKKIE</font></b></label>
  </p></center></span>
      </div>
    </div>
	
	
	 <div class="col s12 m3">
      <div class="card hoverable" id="one">
       <div class="card-image">
          <img src="../images/5-ton1.jpg" style="padding:10px; border-radius:25px; height:165px;">
        </div>
        <div class="card-image">
          <img src="../images/5-ton1.jpg" style="padding:10px; border-radius:25px; height:165px;">   
          <span class="card-title" style="cursor:pointer;" id="more" onClick="viewMore()">View More</span>    
     <a class="btn-floating halfway-fab waves-effect waves-light blue" onClick="openHelp('A 4-5 truck is a pickup truck with a certain curb weight and payload capacity', this)"><i class="material-icons">help</i></a>
        </div>
         <span class="card-title"><center><p style="padding-left:10px;">
    <input class="with-gap" name="struck" type="radio" id="5t" value="4-5 Tonne Truck"/>
    <label for="5t"><b><font color="#000000">4-5 TON</font></b></label>
  </p></center></span>
      </div>
  
    
     
      <div class="card hoverable" id="two">

       <div class="card-image">
          <img src="../images/7-ton1.jpg" style="padding:10px; border-radius:25px; height:165px;">
        </div>
        <div class="card-image">
          <img src="../images/7-ton1.jpg" style="padding:10px; border-radius:25px; height:165px;">   
          <span class="card-title" style="cursor:pointer;" id="less" onClick="viewLess()">View Less</span>    
     <a class="btn-floating halfway-fab waves-effect waves-light blue" onClick="openHelp('A 7-8 truck is a pickup truck with a certain curb weight and payload capacity', this)"><i class="material-icons">help</i></a>
        </div>
         <span class="card-title"><p style="padding-left:10px;">
    <input class="with-gap" name="struck" type="radio" id="7t" value="7-8 Tonne Truck"/>
    <label for="7t"><b><font color="#000000">7-8 TON</font></b></label>
  </p></span>
      </div>
	    </div>  
	<!-- End -->	 
  </div>

  <div class="row">
  <div class="col s12 m12">
  <div class="card" id="selectTruckNextCard" style=" border-style:solid; border-color:#FF0000; ">
  <div class="card-content">
    <span class="card-title" style="align-items: : center; font-size: 17px; line-height: 20px; margin-bottom: 9px; margin-top: 0px;">SELECT THE TRUCK OF YOUR CHOICE AND CLICK NEXT
            <a class="btn waves-effect waves-light right" onClick="stepOne()"  style="background-color:#0D760D; display: inline-block;">
                                           NEXT
                                        </a></span>
 </div>
  </div>
  </div>
</div>
  </form>
  </div>

  
   <!-- <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">help</i></a>
    <!--End Of Cards -->
	
    
	<!-- Dislay Google map here -->
    
   <div class="container" ng-controller="mapController" id="step2" style="width: 98%;">
   <div class="row">
   <div class="col s12 m4 l4">
   <div class="card hoverable">
     <div class="card-title" style="color: #fff; height: 35px; padding-left:10px;background-color:#000; font-size: 14px;">
      Plot you travel <a class="waves-effect waves-light btn modal-trigger btn-floating btn-small red" ng-click="addPlot()" style="cursor:pointer;"><i class="large material-icons">add</i></a> click (+) to add route
      </div>
      <div class="card-content">
		<input type="text" id="address" placeholder="Enter pickup address"/>	
		<input type="text" id="address2" placeholder="Enter destination address"/>   
		<input type="text" id="address3" placeholder="Enter another destination address"/>	
		<input type="text" id="address4" placeholder="Enter another destination address"/>
         <a class="btn waves-effect waves-light" ng-click="resetMap()"  style="background-color:#FFA31A;">
                                Reset
        </a><a class="btn waves-effect waves-light right" ng-click="stepTwo()"  style="background-color:#0D760D; display: inline-block;">
                                           NEXT
                                        </a>
        <div id="hiddedTextFields">
        <input type="text" id="lat1"/>
		<input type="text" id="lng1"/><br/>
        <input type="text" id="lat2" value="0" />
		<input type="text" id="lng2" value="0"/><br/>
        <input type="text" id="lat3" value="0"/>
		<input type="text" id="lng3" value="0"/><br/>
        <input type="text" id="lat4" value="0"/>
		<input type="text" id="lng4" value="0"/><br/>
        </div>
        </div>
	</div>
	   <div class="card hoverable" id="calcDiv">
     <div class="card-title" style="color: #fff; height: 35px; padding-left:10px;background-color:#000; font-size: 14px; padding-top: 7px;">
      Results<i class="material-icons right tooltipped" data-position="bottom" data-delay="50" data-tooltip="Refresh" style="color:#FFFFFF; cursor:pointer; padding-top:6px; padding-right:2px; font-size: 15px;" id="btnRefreshProvider" onClick="updateProviderView()">sync</i>
      </div>
      <div class="card-content">
      <input type="text" id="foundProviders" style="display:none;" value="0" />
<div class="card-panel badge" style=" border-style:solid; border-color:#F00;">
          <span class="text notifyuser" id="distanceFound">Travel Distance:
          </span>
        </div>
        <div class="card-panel badge" style=" border-style:solid; border-color:#F00;">
          <span class="text notifyuser" id="travelTime">Travel Time:
          </span>
        </div>
          <div class="card-panel badge" style=" border-style:solid; border-color:#F00;">
          <span class="text notifyuser" id="providersFound">Available Providers:
          </span>
        </div>
       
        </div>
	</div>
    </div>
    <div class="col s12 m8 l8">
	<div class="card hoverable">
    <div class="card-content">
	<div id='map-canvas' style="width:100%; height:443px;" ></div><br/>
	 
    </div>
   
	</div>
    </div>
    </div>
    </div>

    <!-- enter your details now -->
     <div class="container" id="step3" style="width: 98%;">
    <div class="row">
	 <div class="col s12 m12 l12">
   <div class="card hoverable">
     <div class="card-title" style="color: #fff; height: 35px; padding-left:10px;padding-top:6px;padding-right:6px; background-color:#000; font-size: 14px;">
     Fill Details Below  
      </div>
      <div class="card-content"  style="padding-bottom: 2px;">
	    
		<div class="row">
				<div class="input-field col s12 m6 l6">	    
       <i class="material-icons prefix">work</i>	
        <select id="ltype">
      <option value="" disabled selected>Choose your option</option>
      <option value="House Hold">House Hold</option>
      <option value="Rubble">Rubble</option>
      <option value="Fragiles">Fragiles</option>
      <option value="Cement">Cement</option>
        </select>
        <label><font color="#CCCCCC" face="Palatino Linotype, Book Antiqua, Palatino, serif">Load Type </font></label>
       
		</div>
        <div class="input-field col s12 m6 l6">	    
       <i class="material-icons prefix">work</i>	
        <select id="ltype">
      <option value="" disabled selected>Choose your option</option>
      <option value="Open">Open</option>
      <option value="Closed">Closed</option>
        </select>
        <label><font color="#CCCCCC" face="Palatino Linotype, Book Antiqua, Palatino, serif">Body Type </font></label>      
		</div>

       
        <div class="input-field col s6 m3 l3">
          <i class="material-icons prefix">alarm_on</i>	
		<input type="date" id="pickupd" class= "datepicker"/>
        <label for="pickupd"><font color="#CCCCCC" face="Palatino Linotype, Book Antiqua, Palatino, serif">Pick Up Date</font></label>	 
		</div>
        <div class="input-field col s6 m3 l3">
		<input type="text" id="pickupt" class= "pick-a-time" readonly/>
        <label for="pickupt"><font color="#CCCCCC" face="Palatino Linotype, Book Antiqua, Palatino, serif">Pick Up Time</font></label>	 
		</div>


        <div class="input-field col s6 m3 l3">
          <i class="material-icons prefix">alarm_on</i>	
		<input type="date" id="expire" class= "datepicker"/>
        <label for="expire"><font color="#CCCCCC" face="Palatino Linotype, Book Antiqua, Palatino, serif">Expiry Date</font></label>	 
		</div>
        <div class="input-field col s6 m3 l3">
		<input type="text" id="expiretime" class= "pick-a-time" readonly/>
        <label for="expiretime"><font color="#CCCCCC" face="Palatino Linotype, Book Antiqua, Palatino, serif">Expiry Time</font></label>	 
		</div>

		<div class="input-field col s12 m6 l6">
        <i class="material-icons prefix">person</i>		
		<input type="text" id="fname"/>
        <label for="fname"><font color="#CCCCCC" face="Palatino Linotype, Book Antiqua, Palatino, serif">Your Name</font></label>
		</div>
		<div class="input-field col s12 m6 l6">	
         <i class="material-icons prefix">email</i>		
		<input type="text" id="email"/>
        <label for="email"><font color="#CCCCCC" face="Palatino Linotype, Book Antiqua, Palatino, serif">Your Email</font></label>
		</div>
		
		<div class="input-field col s12 m6 l6">
		 <i class="material-icons prefix">phone</i>	
		<input type="text" id="cell"/>
        <label for="cell"><font color="#CCCCCC" face="Palatino Linotype, Book Antiqua, Palatino, serif">Cell</font></label>
		</div>
        
       
		<div class="input-field col s12 m6 l6">
		 <i class="material-icons prefix">settings_remote</i>	
       <select id="communication">
      <option value="" disabled selected>Choose your option</option>
      <?php
      $query=mysqli_query($con, "select name from soptions");
      while($fetch=mysqli_fetch_array($query)){
        $nam=$fetch['name'];
        ?>
        <option value="<?php echo $nam; ?>"><?php echo $nam; ?></option>
        <?php
      }
      ?>
        </select>
        <label for="communication"><font color="#CCCCCC" face="Palatino Linotype, Book Antiqua, Palatino, serif">Prefered Communication</font></label>
		</div>
		

        <div class="input-field col s12 m12 l12">
		 <i class="material-icons prefix">info_outline</i>	
		<input type="text" id="otherinfor"/>
        <label for="otherinfor"><font color="#CCCCCC" face="Palatino Linotype, Book Antiqua, Palatino, serif">Other Relavant Information </font></label>
		</div>
		  <div class="input-field col s12 m6 l6">
		  <a class="btn waves-effect waves-light right" onClick="sendRequest('Your truck request details have been sent. You will get quote soon', this)"  style="background-color:#0D760D; display: inline-block;">
                                           SUBMIT
                                        </a>
		</div>
        </div>
  </div>
    </div>
	</div>
	</div>
</body>
</html>